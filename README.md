Repozitář pro administrační prostředí aplikace "Webová podpora pro doučování"

# Požadavky
- [Laravel požadavky](https://laravel.com/docs/8.x/deployment#server-requirements)
- [Node.js](https://nodejs.org/en/)
- [Composer](https://getcomposer.org/)

# Instalace
1. Naklonovat repozitář
2. Na serveru nastavit kořenový adresář do složky aplikace public, případně upravit soubor .htaccess
3. V adresáři aplikace spustit příkaz `composer install`
4. V adresáři aplikace spustit příkaz `npm install`
