<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class SettingsSeeder
 * @package Database\Seeders
 * @author Ronald Rebernigg
 * Seeder pro naplnění databáze ukázkovými daty nastavení domovské stránky, provede se po instalaci
 */

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['key' => 'STYLE_SLIDER', 'value' => 'slider.svg'],
            ['key' => 'STYLE_TITLE', 'value' => 'Nadpis hlavní obrazovky'],
            ['key' => 'STYLE_TEXT', 'value' => 'Text hlavní obrazovky'],

            ['key' => 'STYLE_ICON_TITLE', 'value' => 'Nadpis sekce ikon'],
            ['key' => 'STYLE_ICON_1', 'value' => 'fa-book'],
            ['key' => 'STYLE_ICON_1_TITLE', 'value' => 'Nadpis ikona #1'],
            ['key' => 'STYLE_ICON_1_TEXT', 'value' => 'Text ikona #1'],

            ['key' => 'STYLE_ICON_2', 'value' => 'fa-book'],
            ['key' => 'STYLE_ICON_2_TITLE', 'value' => 'Nadpis ikona #2'],
            ['key' => 'STYLE_ICON_2_TEXT', 'value' => 'Text ikona #2'],

            ['key' => 'STYLE_ICON_3', 'value' => 'fa-book'],
            ['key' => 'STYLE_ICON_3_TITLE', 'value' => 'Nadpis ikona #3'],
            ['key' => 'STYLE_ICON_3_TEXT', 'value' => 'Text ikona #3'],

            ['key' => 'STYLE_BIO_TITLE', 'value' => 'Nadpis sekce autor'],
            ['key' => 'STYLE_BIO_TEXT', 'value' => 'Text sekce autor'],
        ];

        Setting::insert($settings);
    }
}
