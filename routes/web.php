<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InstallController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\TermController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/**
 * @author Ronald Rebernigg
 * Soubor s routami aplikace, routy jsou rozděleny podle jednotlivých skupin middlewarů
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/install', [InstallController::class, 'index'])->name('install');
Route::post('/install', [InstallController::class, 'createDatabase']);

Route::middleware(['database'])->group(function (){

    //Admin panel routes
    Route::middleware(['admin'])->group(function (){
        Route::post('/admin/logout', [LogoutController::class, 'logout'])->name('logout');

        Route::post('/upload/wysiwyg', [FileController::class, 'wysiwyg']);

        Route::get('/admin/', [HomeController::class, 'index'])->name('home');

        //User
        Route::get('/admin/users', [UserController::class, 'index'])->name('users');
        Route::get('/admin/users/new', [UserController::class, 'create'])->name('users.create');
        Route::post('/admin/users/new', [UserController::class, 'store']);
        Route::get('/admin/users/show/{user}', [UserController::class, 'show'])->name('users.detail');
        Route::get('/admin/users/edit/{user}', [UserController::class, 'edit'])->name('users.edit');
        Route::post('/admin/users/edit/{user}', [UserController::class, 'update']);
        Route::delete('/admin/users/delete/{user}', [UserController::class, 'delete'])->name('users.delete');

        //Article
        Route::get('/admin/news', [ArticleController::class, 'index'])->name('news');
        Route::get('/admin/news/create', [ArticleController::class, 'create'])->name('news.create');
        Route::post('/admin/news/create', [ArticleController::class, 'store']);
        Route::get('/admin/news/edit/{article}', [ArticleController::class, 'edit'])->name('news.edit');
        Route::post('/admin/news/edit/{article}', [ArticleController::class, 'update']);
        Route::delete('/admin/news/delete/{article}', [ArticleController::class, 'delete'])->name('news.delete');

        //Course
        Route::get('/admin/courses', [CourseController::class, 'index'])->name('courses');
        Route::get('/admin/courses/create', [CourseController::class, 'create'])->name('courses.create');
        Route::post('/admin/courses/create', [CourseController::class, 'store']);
        Route::get('/admin/courses/detail/{course}', [CourseController::class, 'show'])->name('courses.detail');
        Route::get('/admin/courses/edit/{course}', [CourseController::class, 'edit'])->name('courses.edit');
        Route::post('/admin/courses/edit/{course}', [CourseController::class, 'update']);
        Route::delete('/admin/courses/delete/{course}', [CourseController::class, 'delete'])->name('courses.delete');

        //Terms
        Route::get('/admin/terms', [TermController::class, 'index'])->name('terms');
        Route::get('/admin/terms/old', [TermController::class, 'showFinishedTerms'])->name('terms.old');
        Route::get('/admin/terms/create/{course?}', [TermController::class, 'create'])->name('terms.create');
        Route::post('/admin/terms/create', [TermController::class, 'store']);
        Route::get('/admin/terms/detail/{term}', [TermController::class, 'show'])->name('terms.detail');
        Route::get('/admin/terms/edit/{term}', [TermController::class, 'edit'])->name('terms.edit');
        Route::post('/admin/terms/edit/{term}', [TermController::class, 'update']);
        Route::delete('/admin/terms/delete/{term}', [TermController::class, 'delete'])->name('terms.delete');
        Route::post('/admin/terms/{term}/user/attach/', [TermController::class, 'attachUser'])->name('terms.attach_user');
        Route::delete('/admin/terms/{term}/user/detach/{user}', [TermController::class, 'detachUser'])->name('terms.detach_user');

        //Lessons
        Route::get('/admin/lessons/detail/{lesson}', [LessonController::class, 'show'])->name('lessons.detail');
        Route::get('/admin/lessons/create/{course}', [LessonController::class, 'create'])->name('lessons.create');
        Route::post('/admin/lessons/create/{course}', [LessonController::class, 'store']);
        Route::get('/admin/lessons/edit/{lesson}', [LessonController::class, 'edit'])->name('lessons.edit');
        Route::post('/admin/lessons/edit/{lesson}', [LessonController::class, 'update']);
        Route::delete('/admin/lessons/delete/{lesson}', [LessonController::class, 'delete'])->name('lessons.delete');

        //Comments
        Route::get('/admin/comments', [CommentController::class, 'index'])->name('comments');
        Route::get('/admin/comments/detail/{comment}', [CommentController::class, 'show'])->name('comments.detail');
        Route::delete('/admin/comments/delete/{comment}', [CommentController::class, 'delete'])->name('comments.delete');

        //Ratings
        Route::get('/admin/ratings', [RatingController::class, 'index'])->name('ratings');
        Route::get('/admin/ratings/detail/{rating}', [RatingController::class, 'show'])->name('ratings.detail');
        Route::delete('/admin/ratings/delete/{rating}', [RatingController::class, 'delete'])->name('ratings.delete');

        //Payments
        Route::get('/admin/payments', [PaymentController::class, 'index'])->name('payments');
        Route::post('/admin/payments/confirm', [PaymentController::class, 'confirmPayment'])->name('payments.confirm');
        Route::post('/admin/payments/deny', [PaymentController::class, 'denyPayment'])->name('payments.deny');
        Route::delete('/admin/payments/delete/{payment}', [PaymentController::class, 'delete'])->name('payments.delete');

        //Sites
        Route::get('/admin/sites', [SiteController::class, 'index'])->name('sites');
        Route::get('/admin/sites/create', [SiteController::class, 'create'])->name('sites.create');
        Route::post('/admin/sites/create', [SiteController::class, 'store']);
        Route::get('/admin/sites/edit/{site}', [SiteController::class, 'edit'])->name('sites.edit');
        Route::post('/admin/sites/edit/{site}', [SiteController::class, 'update']);
        Route::delete('/admin/sites/delete/{site}', [SiteController::class, 'delete'])->name('sites.delete');

        //Email
        Route::get('/admin/email', [EmailController::class, 'index'])->name('email');
        Route::post('/admin/email', [EmailController::class, 'send']);
        Route::get('/admin/email/{course}/terms', [EmailController::class, 'getTerms']);


        //Settings
        Route::get('/admin/settings', [SettingsController::class, 'index'])->name('settings');
        Route::post('/admin/settings/app', [SettingsController::class, 'storeApp'])->name('settings.app');
        Route::post('/admin/settings/contact', [SettingsController::class, 'storeContact'])->name('settings.contact');
        Route::post('/admin/settings/email', [SettingsController::class, 'storeEmail'])->name('settings.email');
        Route::post('/admin/settings/style', [SettingsController::class, 'storeStyle'])->name('settings.style');
        Route::post('/admin/settings/terms', [SettingsController::class, 'storeTerms'])->name('settings.terms');

        Route::post('/kurzy/{course}/hodnoceni/odpoved/', [\App\Http\Controllers\Web\RatingController::class, 'reply'])->name('web.rating.reply');
        Route::delete('/kurzy/{course}/hodnoceni/odpoved/{reply}/smazat', [\App\Http\Controllers\Web\RatingController::class, 'deleteReply'])->name('web.reply.delete');
    });


    Route::middleware(['guest'])->group(function (){
        //Admin panel auth routes
        Route::get('/admin/register', [RegisterController::class, 'index'])->name('register');
        Route::post('/admin/register', [RegisterController::class, 'register']);
        Route::get('/admin/login',[LoginController::class, 'index'])->name('login');
        Route::post('/admin/login',[LoginController::class, 'login']);

        //Web routes
        Route::get('/login', [LoginController::class, 'indexWeb'])->name('web.login');
        Route::post('/login', [LoginController::class, 'loginWeb']);
        Route::get('/register', [RegisterController::class, 'indexWeb'])->name('web.register');
        Route::post('/register', [RegisterController::class, 'registerWeb']);

        //Forgotten password routes
        Route::get('/forgot-password', [ForgotPasswordController::class, 'index'])->name('password.request');
        Route::post('/forgot-password', [ForgotPasswordController::class, 'passwordResetRequest'])->name('password.email');
        Route::get('/reset-password/{token}', [ForgotPasswordController::class, 'showReset'])->name('password.reset');
        Route::post('/reset-password', [ForgotPasswordController::class, 'passwordReset'])->name('password.update');
    });

    Route::middleware(['auth'])->group(function (){
        Route::post('/logout', [LogoutController::class, 'logoutWeb'])->name('web.logout');

        Route::get('/kurzy/{course}/hodnoceni/nove/', [\App\Http\Controllers\Web\RatingController::class, 'create'])->name('web.rating.new');
        Route::post('/kurzy/{course}/hodnoceni/nove/', [\App\Http\Controllers\Web\RatingController::class, 'store']);
        Route::delete('/kurzy/{course}/hodnoceni/{rating}/smazat', [\App\Http\Controllers\Web\RatingController::class, 'delete'])->name('web.rating.delete');

        Route::get('/lekce/detail/{lesson}', [\App\Http\Controllers\Web\LessonController::class, 'show'])->name('web.lesson.detail');

        Route::get('/uzivatel/', [\App\Http\Controllers\Web\UserController::class, 'index'])->name('web.user');
        Route::get('/uzivatel/editace', [\App\Http\Controllers\Web\UserController::class, 'edit'])->name('web.user.edit');
        Route::post('/uzivatel/editace', [\App\Http\Controllers\Web\UserController::class, 'update']);

        Route::post('/termin/{term}/prihlasit', [\App\Http\Controllers\Web\TermController::class, 'attachUser'])->name('web.course.term.attach');
        Route::post('/termin/{term}/odhlasit', [\App\Http\Controllers\Web\TermController::class, 'detachUser'])->name('web.course.term.detach');

        Route::post('/lekce/{lesson}/komentar/pridat', [\App\Http\Controllers\Web\CommentController::class, 'store'])->name('web.lessons.comment.create');
        Route::delete('/lekce/{lesson}/komentar/{comment}/smazat', [\App\Http\Controllers\Web\CommentController::class, 'delete'])->name('web.lessons.comment.delete');

        Route::get('/platby/{course}', [\App\Http\Controllers\Web\PaymentController::class, 'index'])->name('web.payment');
        Route::get('/platby/detail/{payment}', [\App\Http\Controllers\Web\PaymentController::class, 'show'])->name('web.payment.detail');
    });

    Route::get('/', [\App\Http\Controllers\Web\HomeController::class, 'index'])->name('web.home');
    Route::get('/kurzy', [\App\Http\Controllers\Web\CourseController::class, 'index'])->name('web.courses');
    Route::get('/kurzy/{course}', [\App\Http\Controllers\Web\CourseController::class, 'show'])->name('web.courses.detail');
    Route::get('/novinky', [\App\Http\Controllers\Web\ArticleController::class, 'index'])->name('web.articles');
    Route::get('/novinky/{article}', [\App\Http\Controllers\Web\ArticleController::class, 'show'])->name('web.articles.detail');
    Route::get('/terminy/{course}', [\App\Http\Controllers\Web\TermController::class, 'index'])->name('web.terms');
    Route::get('/lekce/{course}', [\App\Http\Controllers\Web\LessonController::class, 'index'])->name('web.lessons');
    Route::get('/kurzy/{course}/hodnoceni/', [\App\Http\Controllers\Web\RatingController::class, 'index'])->name('web.ratings');
    Route::get('/stranka/{site}', [\App\Http\Controllers\Web\SiteController::class, 'show'])->name('web.site');
    Route::get('/obchodni-podminky', [\App\Http\Controllers\Web\SiteController::class, 'trade'])->name('web.site.trade');
});

