<?php

namespace App\Providers;

use App\Http\View\Composers\FooterComposer;
use App\Http\View\Composers\HeaderComposer;
use App\Http\View\Composers\MainComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewComposerServiceProvider
 * @package App\Providers
 * @author Ronald Rebernigg
 * Provider pro nastavení composerů pro jednotlivé view
 */

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('web.layouts.header', HeaderComposer::class);
        View::composer('web.layouts.main', MainComposer::class);
        View::composer('web.layouts.footer', FooterComposer::class);
    }
}
