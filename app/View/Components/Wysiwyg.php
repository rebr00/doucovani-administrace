<?php

namespace App\View\Components;

use Illuminate\View\Component;

/**
 * Class Wysiwyg
 * @package App\View\Components
 * @author Ronald Rebernigg
 * Komponenta pro view WISYWIG editoru
 */

class Wysiwyg extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.wysiwyg');
    }
}
