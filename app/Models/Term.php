<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Term
 * @package App\Models
 * @author Ronald Rebernigg
 * Model termínu
 */

class Term extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'course_id',
        'date',
        'capacity',
        'address',
        'note',
    ];

    public $sortable = [
        'date',
        'capacity',
        'note',
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
