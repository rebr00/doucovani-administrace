<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Comment
 * @package App\Models
 * @author Ronald Rebernigg
 * Model komentáře
 */

class Comment extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'lesson_id',
        'user_id',
        'parent_id',
        'body',
    ];

    public $sortable = [
        'body',
        'created_at',
    ];

    public function lesson(){
        return $this->belongsTo(Lesson::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getReplies(){
        $replies = Comment::where('parent_id', $this->id)->latest()->get();

        return $replies;
    }
}
