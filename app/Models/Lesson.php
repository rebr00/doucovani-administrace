<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Lesson
 * @package App\Models
 * @author Ronald Rebernigg
 * Model lekce
 */

class Lesson extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'title',
        'body',
        'order',
    ];

    public $sortable = [
        'title',
        'body',
        'order',
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
