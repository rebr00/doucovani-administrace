<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RatingReply
 * @package App\Models
 * @author Ronald Rebernigg
 * Model odpovědi na hodnocení kurzu
 */

class RatingReply extends Model
{
    use HasFactory;

    protected $fillable = [
        'body',
    ];

    public function rating(){
        return $this->belongsTo(Rating::class);
    }
}
