<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Course
 * @package App\Models
 * @author Ronald Rebernigg
 * Model kurzu
 */

class Course extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'name',
        'description',
        'price',
        'lesson',
    ];

    public $sortable = [
        'name',
        'description',
        'price',
        'lesson',
        'created_at',
        'updated_at',
    ];

    public function terms(){
        return $this->hasMany(Term::class);
    }

    public function lessons(){
        return $this->hasMany(Lesson::class);
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }

    public function ratings(){
        return $this->hasMany(Rating::class);
    }
}
