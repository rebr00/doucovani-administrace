<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class User
 * @package App\Models
 * @author Ronald Rebernigg
 * Model uživatele
 */

class User extends Authenticatable
{
    use HasFactory, Notifiable, Sortable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'role',
        'password',
    ];

    public $sortable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'role',
        'password',
        'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Returns true if user has admin role set
     *
     * @return bool
     */
    public function isAdmin(){
        return $this->role === 1;
    }

    /**
     * Returns true if model is signed up for term
     *
     * @param Term $term
     * @return mixed
     */
    public function hasTerm(Term $term){
        $contains = $term->users->contains(Auth::user());

        return $contains;
    }

    public function hasPaidCourse(Course $course){
        $contains = $course->payments
            ->where('status', 1)
            ->where('user_id', Auth::id());

        if(count($contains) !== 0){
            return true;
        }else{
            return false;
        }
    }

    public function hasPayment(Course $course){
        $contains = $course->payments
            ->where('user_id', Auth::id());

        if(count($contains) !== 0){
            return true;
        }else{
            return false;
        }
    }

    public function articles(){
        return $this->hasMany(Article::class);
    }

    public function terms(){
        return $this->belongsToMany(Term::class)->withTimestamps();
    }

    public function payments(){
        return $this->hasMany(Payment::class);
    }

    public function ratings(){
        return $this->hasMany(Rating::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
