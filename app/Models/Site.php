<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Site
 * @package App\Models
 * @author Ronald Rebernigg
 * Model stránek
 */

class Site extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'title',
        'body',
    ];

    public $sortable = [
        'title',
        'body',
        'created_at',
    ];
}
