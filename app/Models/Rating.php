<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Rating
 * @package App\Models
 * @author Ronald Rebernigg
 * Model hodnocení kurzu
 */

class Rating extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'score',
        'text'
    ];

    public $sortable = [
        'score',
        'text',
        'created_at',
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function reply(){
        return $this->hasOne(RatingReply::class);
    }
}
