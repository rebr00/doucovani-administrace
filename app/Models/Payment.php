<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Payment
 * @package App\Models
 * @author Ronald Rebernigg
 * Model platby
 */

class Payment extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'status',
        'reference_number',
    ];

    public $sortable = [
        'id',
        'status',
        'reference_number',
        'created_at',
        'updated_at',
    ];

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
