<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Article
 * @package App\Models
 * @author Ronald Rebernigg
 * Model novinky
 */

class Article extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'title',
        'body',
    ];

    protected $sortable = [
        'title',
        'body',
        'created_at',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
