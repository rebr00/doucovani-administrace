<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Order
 * @package App\Mail
 * @author Ronald Rebernigg
 * Mailable třída, posíla se studentům automaticky při vytvoření platby (za předpokladu, že ne nastaveno SMTP)
 */

class Order extends Mailable
{
    use Queueable, SerializesModels;

    public $qr;
    public $course;
    public $referenceNumber;
    public $user;
    public $bank;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bank, $qr, $course, $referenceNumber, $user)
    {
        $this->subject = 'Platba za kurz: '. $course->name;
        $this->bank = $bank;
        $this->qr = $qr;
        $this->course = $course;
        $this->referenceNumber = $referenceNumber;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.order');
    }
}
