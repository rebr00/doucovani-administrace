<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class ArticleController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro CRUD novinek
 */

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::sortable(['created_at' => 'desc'])->paginate(10);

        return view('news.news', ['articles' => $articles]);
    }

    public function create(){
        return view('news.news_create');
    }

    public function store(Request $request){
        $this->validate($request,[
           'title' => 'required|string',
           'body' => 'required|string',
        ]);

        $request->user()->articles()->create([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        Session::flash('message', 'Novinka byla přidána úspěšně');
        return redirect()->route('news');
    }

    public function edit(Article $article){
        return view('news.news_edit', [
            'article' => $article,
        ]);
    }

    public function update(Article $article, Request $request){
        $this->validate($request,[
            'title' => 'required|string',
            'body' => 'required|string',
        ]);

        $article->title = $request->title;
        $article->body = $request->body;
        $article->save();

        Session::flash('message', 'Změny byly uloženy');
        return redirect()->route('news');
    }

    public function delete(Article $article){
        $article->delete();

        Session::flash('message', 'Novinka byla smazána');
        return redirect()->route('news');
    }
}
