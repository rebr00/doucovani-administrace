<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Setting;
use App\Models\Site;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro zobrazení hlavní stránky
 */

class HomeController extends Controller
{
    public function index(){
        $articleCount = Article::all()
            ->count();
        $articles = Article::latest()
            ->take(3)
            ->get();

        $logo = Setting::where('key', 'APP_LOGO')->first();
        $sites = Site::orderBy('title')->get(['id', 'title']);

        //Získání dat nastavení aplikace z databáze
        $settings = Setting::all();
        $settingsCollection = collect();
        foreach ($settings as $setting){
            $settingsCollection->put($setting->key, $setting->value);
        }

        return view('web.home',[
            'articles' => $articles,
            'count' => $articleCount,
            'settings' => $settingsCollection,
            'logo' => $logo,
            'sites' => $sites,
        ]);
    }
}
