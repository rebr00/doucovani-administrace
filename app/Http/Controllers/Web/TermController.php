<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class TermController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro termíny kurzu na klientské části
 */

class TermController extends Controller
{
    public function index(Course $course){
        $terms = Term::where('course_id', $course->id)
            ->where('date', '>', Carbon::now())
            ->get();

        return view('web.terms', [
            'course' => $course,
            'terms' => $terms,
        ]);
    }

    public function attachUser(Term $term){
        $user = Auth::user();

        if(!$term->users->contains($user)){
            $term->users()->attach($user);
        }

        return back();
    }

    public function detachUser(Term $term){
        $user = Auth::user();

        if($term->users->contains($user)){
            $term->users()->detach($user);
        }

        return back();
    }
}
