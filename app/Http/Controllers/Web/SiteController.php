<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\Site;
use Illuminate\Http\Request;

/**
 * Class SiteController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro zobrazení obsahu vytvořených stránek
 */

class SiteController extends Controller
{
    public function show(Site $site){
        return view('web.site', [
            'site' => $site,
        ]);
    }

    public function trade(){
        $tradeTerms = Setting::where('key', 'SITE_TERMS')->first();

        return view('web.trade_terms', [
            'trade_terms' => $tradeTerms->value,
        ]);
    }
}
