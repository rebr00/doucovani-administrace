<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\Message;
use App\Mail\Order;
use App\Models\Course;
use App\Models\Payment;
use App\Models\Setting;
use Carbon\Carbon;
use Defr\QRPlatba\QRPlatba;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Class PaymentController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro vytvoření nové platby a zobrazení detailu platby
 */

class PaymentController extends Controller
{
    public function index(Course $course){
        if(Auth::user()->hasPayment($course)){
            $payment = Auth::user()->payments->where('course_id', $course->id)->first();

            return redirect()->route('web.payment.detail', $payment);
        }

        $year = substr(Carbon::now()->year, -2);
        $minute = substr(Carbon::now()->minute, -2);
        $user = substr(Auth::id(), -3);
        $crs = substr($course->id, -3);

        $referenceNumber = (int)($year.''.$minute.''.$crs.''.$user);

        $payment = new Payment();
        $payment->course()->associate($course);
        $payment->user()->associate(Auth::user());
        $payment->status = 0;
        $payment->reference_number = $referenceNumber;
        $payment->amount = $course->price;
        $payment->save();

        $bankAccount = Setting::where('key', 'APP_BANK')
            ->first();

        $qr = new QRPlatba();
        $qr->setAccount($bankAccount->value)
            ->setVariableSymbol($referenceNumber)
            ->setAmount($payment->amount)
            ->setMessage('Platba za kurz: '.$course->name.','. $payment->user->first_name.' '.$payment->user->last_name)
            ->setCurrency('CZK') // Výchozí je CZK, lze zadat jakýkoli ISO kód měny
            ->setDueDate(Carbon::now()->addWeek());


        $emailSettings = Setting::where('key', 'like', 'EMAIL%')
            ->get();
        if(count($emailSettings) !== 0){
            Mail::to($payment->user->email)->send(new Order($bankAccount, $qr, $course, $referenceNumber, $payment->user));
        }

        return view('web.payment',[
            'qr' => $qr,
            'reference_number' => $referenceNumber,
            'payment' => $payment,
            'account' =>$bankAccount,
            'course' => $course,
        ]);
    }

    public function show(Payment $payment){
        $course = $payment->course;

        $year = substr(Carbon::now()->year, -2);
        $minute = substr(Carbon::now()->minute, -2);
        $user = substr(Auth::id(), -3);
        $crs = substr($course->id, -3);

        $referenceNumber = (int)($year.''.$minute.''.$crs.''.$user);

        $bankAccount = Setting::where('key', 'APP_BANK')
            ->first();

        $qr = new QRPlatba();
        $qr->setAccount($bankAccount->value)
            ->setVariableSymbol($referenceNumber)
            ->setAmount($payment->amount)
            ->setMessage('Platba za kurz: '.$course->name.','. $payment->user->first_name.' '.$payment->user->last_name)
            ->setCurrency('CZK') // Výchozí je CZK, lze zadat jakýkoli ISO kód měny
            ->setDueDate(Carbon::now()->addWeek());

        return view('web.payment_detail',[
            'qr' => $qr,
            'reference_number' => $referenceNumber,
            'account' =>$bankAccount,
            'payment' => $payment,
            'course' => $course,
        ]);
    }
}
