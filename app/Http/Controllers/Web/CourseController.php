<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class CourseController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro zobrazení kurzů na klientské části
 */

class CourseController extends Controller
{
    public function index(){
        $courses = Course::orderBy('name')
            ->paginate(4);

        return view('web.courses', [
            'courses' => $courses,
        ]);
    }

    public function show(Course $course){
        $terms = Term::where('course_id', $course->id)
            ->where('date', '>', Carbon::now())
            ->get();

        return view('web.course_detail', [
            'course' => $course,
            'terms' => $terms,
        ]);
    }
}
