<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Rating;
use App\Models\RatingReply;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class RatingController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro hodnocení kurzu
 */

class RatingController extends Controller
{
    public function index(Course $course){
        $ratings = Rating::where('course_id', $course->id)
            ->latest()
            ->paginate(4);

        return view('web.ratings', [
            'ratings' => $ratings,
            'course' => $course,
        ]);
    }

    public function create(Course $course){
        return view('web.rating_new', [
            'course' => $course,
        ]);
    }

    public function store(Course $course, Request $request){
        $this->validate($request, [
            'score' => 'required|numeric|min:1|max:5',
            'text' => 'required|string'
        ]);

        $rating = new Rating();
        $rating->course()->associate($course);
        $rating->user()->associate(Auth::user());
        $rating->score = $request->score;
        $rating->text = $request->text;
        $rating->save();

        Session::flash('message', 'Vaše hondnocení bylo uloženo úspěšně');
        return redirect()->route('web.ratings', $course);
    }

    public function reply(Course $course, Request $request){
        $this->validate($request, [
            'reply' => 'required|string',
        ]);

        $rating = Rating::find($request->rating);
        if($rating === null){
            Session::flash('message', 'Někde se stala chyba');
            return redirect()->route('web.ratings', $course);
        }

        $reply = new RatingReply();
        $reply->rating()->associate($rating);
        $reply->body = $request->reply;
        $reply->save();

        Session::flash('message', 'Odpověd uložena');
        return redirect()->route('web.ratings', $course);
    }

    public function delete(Course $course, Rating $rating){
        $rating->delete();

        Session::flash('message', 'Hodnocení bylo smazáno');
        return redirect()->route('web.ratings', $course);
    }

    public function deleteReply(Course $course, RatingReply $reply){
        $reply->delete();

        Session::flash('message', 'Hodnocení bylo smazáno');
        return redirect()->route('web.ratings', $course);
    }
}
