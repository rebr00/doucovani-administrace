<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

/**
 * Class UserController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro uživatelský profil na klientské části
 */

class UserController extends Controller
{

    public function index()
    {
        $payments = Payment::where('user_id', Auth::id())
            ->latest()
            ->paginate(5);

        return view('web.user_detail', [
            'user' => Auth::user(),
            'payments' => $payments
        ]);
    }

    public function edit(){
        $user = Auth::user();

        return view('web.user_edit', [
            'user' => $user,
        ]);
    }

    public function update(Request $request){
        $user = User::where('id',Auth::id())->first();

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,'. $user->id .',id',
            'phone' => 'nullable|numeric',
            'password' => 'sometimes|confirmed',
        ]);

        if($request->old_password !== null && $request->password !== null){
            if(Hash::check($request->old_password, $user->password)){
                $user->password = Hash::make($request->password);
            }else{
                Session::flash('message', 'Zadané heslo se neshoduje se stávájícím.');
                return back();
            }
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->save();

        Session::flash('message', 'Změny uloženy.');
        return redirect()->route('web.user');
    }
}
