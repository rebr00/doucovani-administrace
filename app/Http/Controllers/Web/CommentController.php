<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class CommentController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro uložení a smazáni komentáře k lekci
 */

class CommentController extends Controller
{
    public function store(Lesson $lesson, Request $request){
        $this->validate($request, [
            'comment' => 'required|string',
        ]);

        $comment = new Comment();
        $comment->lesson()->associate($lesson);
        $comment->user()->associate(Auth::user());
        $comment->body = $request->comment;
        $comment->parent_id = ($request->comment_parrent !== null) ? $request->comment_parrent : null;
        $comment->save();

        Session::flash('message', 'Komentář byl přidán úspěšně');
        return redirect()->route('web.lesson.detail', $lesson);
    }

    public function delete(Lesson $lesson, Comment $comment){
        $children = Comment::where('parent_id', $comment->id)->get();
        if($children !== null){
            foreach ($children as $child){
                $child->delete();
            }
        }

        $comment->delete();

        Session::flash('message', 'Komentář byl odstraněn.');
        return redirect()->route('web.lesson.detail', $lesson);
    }
}
