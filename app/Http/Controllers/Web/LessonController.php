<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Http\Request;

/**
 * Class LessonController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro zobrazení lekcí (seznam lekcí, které kurz obsahuje a detail lekce)
 */

class LessonController extends Controller
{
    public function index(Course $course){
        return view('web.lessons', [
            'course' => $course
        ]);
    }

    public function show(Lesson $lesson){
        $previous = Lesson::where('order', '<', $lesson->order)
            ->orderByDesc('order')
            ->first();
        $next = Lesson::where('order', '>', $lesson->order)
            ->orderBy('order')
            ->first();

        $comments = Comment::where('lesson_id', $lesson->id)
            ->where('parent_id', null)
            ->latest()
            ->paginate(5);

        return view('web.lesson_detail', [
            'lesson' => $lesson,
            'previous' => $previous,
            'next' => $next,
            'comments' => $comments,
        ]);
    }
}
