<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

/**
 * D
 * Class ArticleController
 * @package App\Http\Controllers\Web
 * @author Ronald Rebernigg
 * Controller pro zobrazení novinek na klientské části
 */

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::latest()->paginate(3);

        return view('web.articles',[
            'articles' => $articles,
        ]);
    }

    public function show(Article $article){
        return view('web.article_detail', [
            'article' => $article,
        ]);
    }
}
