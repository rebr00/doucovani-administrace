<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller domovské stránky, na které jsou zobrazeny statistiky a poslední platby
 */

class HomeController extends Controller
{
    public function index(){
        $firstOfMonth = Carbon::now()->firstOfMonth();
        $lastOfMonth = Carbon::now()->lastOfMonth();

        $firstOfYear = Carbon::now()->firstOfYear();
        $lastOfYear = Carbon::now()->lastOfYear();

        $newUsers = User::whereBetween('created_at', [$firstOfMonth, $lastOfMonth])
            ->count();
        $usersTotal = User::all()
            ->count();

        $financeMonth = Course::whereHas( 'payments', function ($x) use ($firstOfMonth, $lastOfMonth) {
            $x->whereBetween('created_at', [$firstOfMonth, $lastOfMonth])
                ->where('status', 1);
        })
            ->sum('price');

        $financeYear = Course::whereHas( 'payments', function ($x) use ($firstOfYear, $lastOfYear) {
            $x->whereBetween('created_at', [$firstOfYear, $lastOfYear])
                ->where('status', 1);
        })
            ->sum('price');

        $paymentsMonth = Payment::where('status', 1)
            ->whereBetween('created_at', [$firstOfMonth, $lastOfMonth])
            ->count();

        $paymentsYear = Payment::where('status', 1)
            ->whereBetween('created_at', [$firstOfYear, $lastOfYear])
            ->count();

        $payments = Payment::latest()
            ->take(10)
            ->get();

        $fmt = new \NumberFormatter('cs', \NumberFormatter::CURRENCY);

        return view('home',[
            'users' => $newUsers,
            'usersTotal' => $usersTotal,
            'financeMonth' => $financeMonth,
            'financeYear' => $financeYear,
            'paymentsMonth' => $paymentsMonth,
            'paymentsYear' => $paymentsYear,
            'payments' => $payments,
            'fmt' => $fmt,
        ]);
    }
}
