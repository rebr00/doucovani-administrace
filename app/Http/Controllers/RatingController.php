<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use Illuminate\Http\Request;

/**
 * Class RatingController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro zobrazení a smazání hodnocení
 */

class RatingController extends Controller
{
    public function index(){
        $ratings = Rating::sortable(['created_at' => 'desc'])
            ->paginate(10);

        return view('courses.ratings', [
            'ratings' => $ratings,
        ]);
    }

    public function delete(Rating $rating){
        $rating->delete();

        return redirect()->route('ratings');
    }
}
