<?php

namespace App\Http\Controllers;

use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class SiteController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro CRUD nových stránek
 */

class SiteController extends Controller
{
    public function index(){
        $sites = Site::orderBy('title')
            ->sortable()
            ->paginate(10);
        return view('sites.sites', [
            'sites' => $sites,
        ]);
    }

    public function create(){
        return view('sites.site_create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required|string',
            'body' => 'required|string',
        ]);

        Site::create([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        Session::flash('message', 'Stránka byla vytvořena úspěšně. Nyní se zobrazuje na webu.');
        return redirect()->route('sites');
    }

    public function edit(Site $site){
        return view('sites.site_edit', [
            'site' => $site,
        ]);
    }

    public function update(Site $site, Request $request){
        $this->validate($request, [
            'title' => 'required|string',
            'body' => 'required|string',
        ]);

        $site->title = $request->title;
        $site->body = $request->body;
        $site->save();

        Session::flash('message', 'Změny byly uloženy.');
        return redirect()->route('sites');
    }

    public function delete(Site $site){
        $site->delete();

        Session::flash('message', 'Stránka byla odstraněna.');
        return redirect()->route('sites');
    }
}
