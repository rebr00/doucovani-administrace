<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro zpracování souborů nahraných přes WISYWIG editor
 */

class FileController extends Controller
{
    public function wysiwyg(Request $request){
        $validatedData = $request->validate([
            'file' => 'required|file',
        ]);

        $file = Storage::disk('public_root')->putFileAs('files', $request->file('file'), $request->file('file')->getClientOriginalName());

        return ['location' => url($file)];
    }
}
