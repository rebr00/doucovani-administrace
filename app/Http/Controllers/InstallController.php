<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use function PHPUnit\Framework\exactly;

/**
 * Class InstallController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro instalaci aplikace, nastavení uloženo do DB a .env
 */

class InstallController extends Controller
{
    public function index(){
        return view('install.home');
    }

    public function createDatabase(Request $request){
        $this->validate($request, [
            'appname' => 'required',
            'appurl' => 'required',
            'hostname' => 'required',
            'hostport' => 'required|numeric',
            'username' => 'required',
            'dbname' => 'required',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed'
        ]);

        if(!$this->checkDatabase($request)){
            Session::flash('message', 'Špatně zadané údaje k databázi. Nelze se připojit.');
            return redirect()->back()
                ->withInput($request->input());
        }

        if(!$this->writeEnv($request)){
            Session::flash('message', 'Při vytváření konfigurace nastala chyba');
            return redirect()->route('install')
                ->withInput($request->input());
        }

        Artisan::call('cache:clear');
        Artisan::call('config:cache');
        DB::reconnect('mysql');

        if(!$this->migrate()){
            Session::flash('message', 'Při vytváření struktury databáze nastala chyba.');
            return redirect()->route('install')
                ->withInput($request->input());
        }

        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 1
        ]);

        Setting::create([
            'key' => 'APP_NAME',
            'value' => $request->appname,
        ]);
        Artisan::call('db:seed');

        return redirect()->route('login');
    }

    public function checkDatabase(Request $request){
        try {
            $db = new \PDO('mysql:host='.$request->hostname.';port='. $request->hostport .';dbname='.$request->dbname, $request->username, $request->db_password);
            return true;
        }catch (\PDOException $e){
            return false;
        }
    }

    public function writeEnv(Request $request){
        $appname = $request->appname;
        $appurl = $request->appurl;
        $hostname = $request->hostname;
        $port = $request->hostport;
        $username = $request->username;
        $password = $request->db_password;
        $dbname = $request->dbname;

        $newEnvString = "\nINSTALLED=true\nAPP_NAME=".$appname."\nAPP_URL=". $appurl ."\nDB_HOST=". $hostname."\nDB_PORT=". $port. "\nDB_DATABASE=". $dbname. "\nDB_USERNAME=". $username. "\nDB_PASSWORD=". $password;

        $env = null;
        if(file_exists(base_path('.env'))){
            $env = file_get_contents(base_path('.env'));
        };

        $envArray = explode("\n", $env);
        $keys = ['APP_NAME', 'APP_URL', 'DB_HOST', 'DB_PORT', 'DB_DATABASE', 'DB_USERNAME', 'DB_PASSWORD', 'INSTALLED'];
        foreach ($keys as $key){
            $unsetKey = key(preg_grep('/^'. $key .'/i', $envArray));
            unset($envArray[$unsetKey]);
        }
        $envUnset = implode("\n", $envArray);
        $envNew = $envUnset.$newEnvString;

        try {
            file_put_contents(base_path('.env'), $envNew);
            return true;
        } catch (\Exception $e){
            return false;
        }
    }

    public function migrate(){
        try{
            Artisan::call('migrate', ["--force"=> true ]);
            return true;
        }
        catch(Exception $e){
            return false;
        }
    }
}
