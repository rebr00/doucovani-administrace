<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

/**
 * Class UserController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro CRUD uživatelů
 */

class UserController extends Controller
{
    public function index(){
        $users = User::sortable()
            ->paginate(10);

        return view('users.users', [
            'users' => $users
        ]);
    }

    public function show(User $user){
        return view('users.users_detail', [
            'user' => $user,
        ]);
    }

    public function create(Request $request){
        return view('users.users_create');
    }

    public function store(Request $request){
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users|max:255',
            'phone' => 'nullable|numeric|digits:9',
            'password' => 'required|confirmed',
            'role' => 'required',
        ]);

        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'role' => $request->role,
        ]);

        Session::flash('message', 'Uživatel byl úspěšně vytvořen');
        return redirect()->route('users');
    }

    public function edit(User $user){
        return view('users.users_edit', [
            'user' => $user,
        ]);
    }

    public function update(User $user, Request $request){
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,'. $user->id .',id',
            'phone' => 'nullable|numeric',
            'role' => 'required',
            'password' => 'sometimes|confirmed',
        ]);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->role = $request->role;

        $user->password = $request->password !== null ? Hash::make($request->password) : $user->password;

        $user->save();

        Session::flash('message', 'Změny uloženy.');
        return redirect()->route('users');

    }

    public function delete(User $user){

        if($user->role === 1){
            $adminCount = User::where('role', 1)
                ->count();

            if($adminCount <=1 ){
                Session::flash('message', 'Nelze odstranit jediný administrátorský účet!');
                return redirect()->route('users');
            }
        }
        $user->delete();

        Session::flash('message', 'Uživatel byl odstraněn.');
        return redirect()->route('users');
    }
}
