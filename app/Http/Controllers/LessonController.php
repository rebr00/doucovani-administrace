<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class LessonController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro CRUD lekcí
 */

class LessonController extends Controller
{
    public function show(Lesson $lesson){
        return view('courses.lessons_detail', [
            'lesson' => $lesson,
        ]);
    }

    public function create(Course $course){
        return view('courses.lessons_create', [
            'course' => $course,
        ]);
    }

    public function store(Course $course, Request $request){
        $this->validate($request, [
            'title' => 'required|string',
            'body' => 'required|string',
            'order' => 'required|numeric',
        ]);

        if($request->hasFile('attach')){
            foreach ($request->file('attach') as $file){
                $fileUpload = Storage::disk('public_root')->putFileAs('files', $file , $file->getClientOriginalName());
                $fileName = $file->getClientOriginalName();
                $fileLink = url($fileUpload);
                $request->body = $request->body.'<br><a class="btn btn-primary my-2" target="_blank" download href="'. $fileLink .'"> <i class="bi bi-download"></i> '. $fileName. '</a>';
            }
        }

        $course->lessons()->create([
            'title' => $request->title,
            'body' => $request->body,
            'order' => $request->order,
        ]);

        Session::flash('message', 'Lekce vytvořena');
        return redirect()->route('courses.detail', $course);
    }

    public function edit(Lesson $lesson){
        return view('courses.lessons_edit', [
            'lesson' => $lesson,
        ]);
    }

    public function update(Lesson $lesson, Request $request){
        $this->validate($request, [
            'title' => 'required|string',
            'body' => 'required|string',
            'order' => 'required|numeric',
        ]);

        if($request->hasFile('attach')){
            foreach ($request->file('attach') as $file){
                $fileUpload = Storage::disk('public_root')->putFileAs('files', $file , $file->getClientOriginalName());
                $fileName = $file->getClientOriginalName();
                $fileLink = url($fileUpload);
                $request->body = $request->body.'<br><a class="btn btn-primary my-2" target="_blank" download href="'. $fileLink .'"> <i class="bi bi-download"></i> '. $fileName. '</a>';
            }
        }

        $lesson->title = $request->title;
        $lesson->body = $request->body;
        $lesson->order = $request->order;
        $lesson->save();

        Session::flash('message', 'Lekce změněna');
        return redirect()->route('courses.detail', $lesson->course);
    }

    public function delete(Lesson $lesson){
        $course = $lesson->course;
        $lesson->delete();

        Session::flash('message', 'Lekce smazána');
        return redirect()->route('courses.detail', $course);
    }


}
