<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

/**
 * Class CourseController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro CRUD kurzů
 */

class CourseController extends Controller
{
    public function index(){
        $courses = Course::sortable()->paginate(10);

        return view('courses.courses', ['courses' => $courses]);
    }

    public function create(){
        $bankSet = Setting::where('key','APP_BANK')
            ->first();
        if($bankSet === null){
            $bankSet = null;
        }else{
            $bankSet = $bankSet->value;
        }

        return view('courses.courses_create',[
            'bank' => $bankSet,
        ]);
    }

    public function store(Request $request){
        $this->validate($request,[
            'image' => 'nullable|file|image',
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        $lesson = $request->lesson ? true : false;

        $course = Course::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'lesson' => $lesson,
        ]);

        if($request->image !== null){
            Storage::disk('public_root')->putFileAs('files/img/course', $request->file('image'), $course->id.'.'.$request->file('image')->extension());
            $course->image = $course->id.'.'.$request->file('image')->extension();
            $course->save();
        }

        Session::flash('message', 'Kurz byl úspěšně vytvořen');
        return redirect()->route('courses');
    }

    public function show(Course $course){
        $terms = $course->terms()
            ->where('date', '>=', Carbon::now())
            ->latest('date')
            ->get();

        $lessons = $course->lessons()
            ->sortable('order')
            ->get();

        return view('courses.courses_detail', [
            'course' => $course,
            'terms' => $terms,
            'lessons' => $lessons,
        ]);
    }

    public function edit(Course $course){
        $bankSet = Setting::where('key','APP_BANK')
            ->first();

        return view('courses.courses_edit', [
            'course' => $course,
            'bank' => $bankSet,
        ]);
    }

    public function update(Course $course, Request $request){
        $this->validate($request,[
            'image' => 'nullable|file|image',
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        $lesson = $request->lesson ? true : false;

        if($request->image !== null){
            if($course->image !== null){
                Storage::disk('public_root')->delete('files/img/course/'.$course->image);
            }
            Storage::disk('public_root')->putFileAs('files/img/course', $request->file('image'), $course->id.'.'.$request->file('image')->extension());
            $course->image = $course->id.'.'.$request->file('image')->extension();
        }

        $course->name = $request->name;
        $course->description = $request->description;
        $course->lesson = $lesson;
        $course->price = $request->price;
        $course->save();

        Session::flash('message', 'Změny byly uloženy');
        return redirect()->route('courses');
    }

    public function delete(Course $course){
        if($course->image !== null){
            Storage::disk('public')->delete('img/course/'.$course->image);
        }

        $course->delete();

        Session::flash('message', 'Kurz byl smazán');
        return redirect()->route('courses');
    }
}
