<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro nastavení aplikace v administraci
 */

class SettingsController extends Controller
{
    public function index(){
        $settings = Setting::all();
        $settingsCollection = collect();

        $bankAccount = $settings->where('key', 'APP_BANK')->first();
        $accNumber = null;
        $code = null;
        $prefix = null;
        if($bankAccount !== null){
            $prefix = str_contains($bankAccount->value, '-') ? explode('-', $bankAccount->value) : null;
            $code = str_contains($bankAccount->value, '/') ? explode('/', $bankAccount->value) : null;
            $accNumber = strpos($bankAccount->value, '-') ? substr($bankAccount->value, strpos($bankAccount->value, '-') +1, 10) : substr($bankAccount->value,0,10);
        }

        $mailHost = config('mail.mailers.smtp.host');
        $mailEncryption = config('mail.mailers.smtp.encryption');
        $mailPort = config('mail.mailers.smtp.port');

        foreach ($settings as $setting){
            $settingsCollection->put($setting->key, $setting->value);
        }

        return view('settings', [
            'settings' => $settingsCollection,
            'bankNumber' => $accNumber,
            'bankCode' => $code,
            'bankPrefix' => $prefix,
            'mail_host' => $mailHost,
            'mail_enc' => $mailEncryption,
            'mail_port' => $mailPort,
        ]);
    }

    public function storeApp(Request $request){
        $this->validate($request, [
            'app_name' => 'required',
            'app_logo' => 'image',
            'app_bank_prefix' => 'nullable|numeric',
            'app_bank' => 'nullable|size:10',
            'app_bank_code' => 'nullable|numeric|digits:4'
        ]);

        $appName = Setting::where('key','APP_NAME')
            ->first();

        if(!($appName->value === $request->app_name)){
            $values = collect();
            $values->put('APP_NAME', $request->app_name);

            $this->writeEnv($values);
            $appName->value = $request->app_name;
            $appName->save();
        }

        if($request->app_logo !== null){
            $appLogo = Setting::where('key', 'APP_LOGO')->first();

            if($appLogo === null){
                Storage::disk('public_root')->putFileAs('files/img/logo', $request->file('app_logo'), 'logo.'. $request->file('app_logo')->extension());
                Setting::create(['key' => 'APP_LOGO', 'value' => 'logo.'.$request->file('app_logo')->extension(),]);
            }else{
                Storage::disk('public_root')->delete('files/img/logo/'.$appLogo->value);
                Storage::disk('public_root')->putFileAs('files/img/logo', $request->file('app_logo'), 'logo.'. $request->file('app_logo')->extension());
                $appLogo->value = 'logo.'.$request->file('app_logo')->extension();
                $appLogo->save();
            }
        }

        if($request->app_icon !== null){
            if($request->file('app_icon')->extension() == 'ico'){
                $appLogo = Setting::where('key', 'APP_ICON')->first();
                if($appLogo === null){
                    Setting::create(['key' => 'APP_ICON', 'value' => 'favicon.ico',]);
                }
                Storage::disk('public_root')->putFileAs('', $request->file('app_icon'), 'favicon.ico');
            }else{
                Session::flash('message', 'Soubor ikony webu musí být typu .ico');
                return redirect()->route('settings');
            }
        }

        if($request->app_bank !== null){
            if($request->app_bank_code === null){
                Session::flash('message', 'Pro nastavení bankovního účtu musí být vyplněno jeho číslo společně s kódem banky.');
                return redirect()->route('settings');
            }
            $appBank = Setting::where('key', 'APP_BANK')->first();

            $accNumber = $request->app_bank_prefix ? $request->app_bank_prefix.'-'.$request->app_bank.'/'.$request->app_bank_code : $request->app_bank.'/'.$request->app_bank_code;

            if($appBank === null){
                Setting::create(['key' => 'APP_BANK', 'value' => $accNumber,]);
            }else{
                $appBank->value = $accNumber;
                $appBank->save();
            }
        }

        Session::flash('message', 'Nastavení aplikace bylo uloženo');

        return redirect()->route('settings');
    }

    public function storeContact(Request $request){
        $address = Setting::where('key','CONTACT_ADDRESS')->first();
        $person = Setting::where('key','CONTACT_PERSON')->first();
        $phone = Setting::where('key','CONTACT_PHONE')->first();
        $fb = Setting::where('key','CONTACT_FACEBOOK')->first();
        $ig = Setting::where('key','CONTACT_INSTAGRAM')->first();
        $yt = Setting::where('key','CONTACT_YOUTUBE')->first();
        $li = Setting::where('key','CONTACT_LINKEDIN')->first();

        if($request->contact_address !== null){
            if($address !== null){
                $address->value = $request->contact_address;
                $address->save();
            }else{
                Setting::create(['key' => 'CONTACT_ADDRESS', 'value' => $request->contact_address,]);
            }
        }
        if($request->contact_person !== null){
            if($person !== null){
                $person->value = $request->contact_person;
                $person->save();
            }else{
                Setting::create(['key' => 'CONTACT_PERSON', 'value' => $request->contact_person,]);
            }
        }
        if($request->contact_phone !== null){
            if($phone !== null){
                $phone->value = $request->contact_phone;
                $phone->save();
            }else{
                Setting::create(['key' => 'CONTACT_PHONE', 'value' => $request->contact_phone]);
            }
        }
        if($request->contact_facebook !== null){
            if($fb !== null){
                $fb->value = $request->contact_facebook;
                $fb->save();
            }else{
                Setting::create(['key' => 'CONTACT_FACEBOOK', 'value' => $request->contact_facebook]);
            }
        }
        if($request->contact_instagram !== null){
            if($ig !== null){
                $ig->value = $request->contact_instagram;
                $ig->save();
            }else{
                Setting::create(['key' => 'CONTACT_INSTAGRAM', 'value' => $request->contact_instagram]);
            }
        }
        if($request->contact_youtube !== null){
            if($yt !== null){
                $yt->value = $request->contact_youtube;
                $yt->save();
            }else{
                Setting::create(['key' => 'CONTACT_YOUTUBE', 'value' => $request->contact_youtube,]);
            }
        }
        if($request->contact_linkedin !== null){
            if($li !== null){
                $li->value = $request->contact_linkedin;
                $li->save();
            }else{
                Setting::create(['key' => 'CONTACT_LINKEDIN', 'value' => $request->contact_linkedin,]);
            }
        }

        Session::flash('message', 'Kontaktní informace byly uloženy.');
        return redirect()->route('settings');
    }

    public function storeEmail(Request $request){
        $this->validate($request, [
            'mail_address' => 'required',
            'mail_password' => 'required',
            'mail_host' => 'required',
            'mail_port' => 'required',
        ]);

        $email = Setting::where('key', 'EMAIL_ADDRESS')->first();

        if($email === null){
            Setting::create(['key' => 'EMAIL_ADDRESS', 'value' => $request->mail_address]);
        }else{
            $email->value = $request->mail_address;
            $email->save();
        }

        $values = collect();
        $values->put('MAIL_USERNAME', $request->mail_address);
        $values->put('MAIL_PASSWORD', $request->mail_password);
        $values->put('MAIL_HOST', $request->mail_host);
        $values->put('MAIL_ENCRYPTION', $request->mail_encryption);
        $values->put('MAIL_PORT', $request->mail_port);

        $this->writeEnv($values);

        Session::flash('message', 'Nastavení emailu uloženo.');
        return redirect()->route('settings');
    }

    public function storeStyle(Request $request){
        if($request->style_slider !== null){
            $style = Setting::where('key', 'STYLE_SLIDER')->first();

            if($style === null){
                Storage::disk('public_root')->putFileAs('files/img/slider', $request->file('style_slider'), 'slider.'. $request->file('style_slider')->extension());
                Setting::create(['key' => 'STYLE_SLIDER', 'value' => 'slider.'.$request->file('style_slider')->extension(),]);
            }else{
                Storage::disk('public_root')->delete('files/img/slider/'.$style->value);
                Storage::disk('public_root')->putFileAs('files/img/slider', $request->file('style_slider'), 'slider.'. $request->file('style_slider')->extension());
                $style->value = 'slider.'.$request->file('style_slider')->extension();
                $style->save();
            }
        }

        if($request->style_title !== null){
            $styleTitle = Setting::where('key', 'STYLE_TITLE')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_TITLE', 'value' => $request->style_title]);
            }else{
                $styleTitle->value = $request->style_title;
                $styleTitle->save();
            }
        }
        if($request->style_text !== null){
            $styleTitle = Setting::where('key', 'STYLE_TEXT')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_TEXT', 'value' => $request->style_text]);
            }else{
                $styleTitle->value = $request->style_text;
                $styleTitle->save();
            }
        }
        if($request->style_icon_title !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_TITLE')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_TITLE', 'value' => $request->style_icon_title]);
            }else{
                $styleTitle->value = $request->style_icon_title;
                $styleTitle->save();
            }
        }

        if($request->style_icon_1 !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_1')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_1', 'value' => $request->style_icon_1]);
            }else{
                $styleTitle->value = $request->style_icon_1;
                $styleTitle->save();
            }
        }
        if($request->style_icon_1_title !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_1_TITLE')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_1_TITLE', 'value' => $request->style_icon_1_title]);
            }else{
                $styleTitle->value = $request->style_icon_1_title;
                $styleTitle->save();
            }
        }
        if($request->style_icon_1_text !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_1_TEXT')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_1_TEXT', 'value' => $request->style_icon_1_text]);
            }else{
                $styleTitle->value = $request->style_icon_1_text;
                $styleTitle->save();
            }
        }
        if($request->style_icon_2 !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_2')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_2', 'value' => $request->style_icon_2]);
            }else{
                $styleTitle->value = $request->style_icon_2;
                $styleTitle->save();
            }
        }
        if($request->style_icon_2_title !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_2_TITLE')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_2_TITLE', 'value' => $request->style_icon_2_title]);
            }else{
                $styleTitle->value = $request->style_icon_2_title;
                $styleTitle->save();
            }
        }
        if($request->style_icon_2_text !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_2_TEXT')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_2_TEXT', 'value' => $request->style_icon_2_text]);
            }else{
                $styleTitle->value = $request->style_icon_2_text;
                $styleTitle->save();
            }
        }
        if($request->style_icon_3 !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_3')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_3', 'value' => $request->style_icon_3]);
            }else{
                $styleTitle->value = $request->style_icon_3;
                $styleTitle->save();
            }
        }
        if($request->style_icon_3_title !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_3_TITLE')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_3_TITLE', 'value' => $request->style_icon_3_title]);
            }else{
                $styleTitle->value = $request->style_icon_3_title;
                $styleTitle->save();
            }
        }
        if($request->style_icon_3_text !== null){
            $styleTitle = Setting::where('key', 'STYLE_ICON_3_TEXT')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_ICON_3_TEXT', 'value' => $request->style_icon_3_text]);
            }else{
                $styleTitle->value = $request->style_icon_3_text;
                $styleTitle->save();
            }
        }

        if($request->style_bio_photo !== null){
            $appLogo = Setting::where('key', 'STYLE_BIO_PHOTO')->first();

            if($appLogo === null){
                Storage::disk('public_root')->putFileAs('files/img/bio', $request->file('style_bio_photo'), 'bio.'. $request->file('style_bio_photo')->extension());
                Setting::create([
                    'key' => 'STYLE_BIO_PHOTO',
                    'value' => 'bio.'.$request->file('style_bio_photo')->extension(),
                ]);
            }else{
                Storage::disk('public_root')->delete('files/img/bio/'. $appLogo->value);
                Storage::disk('public_root')->putFileAs('files/img/bio', $request->file('style_bio_photo'), 'bio.'. $request->file('style_bio_photo')->extension());
                $appLogo->value = 'bio.'.$request->file('style_bio_photo')->extension();
                $appLogo->save();
            }
        }

        if($request->style_bio_text !== null){
            $styleTitle = Setting::where('key', 'STYLE_BIO_TEXT')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_BIO_TEXT', 'value' => $request->style_bio_text]);
            }else{
                $styleTitle->value = $request->style_bio_text;
                $styleTitle->save();
            }
        }
        if($request->style_bio_title !== null){
            $styleTitle = Setting::where('key', 'STYLE_BIO_TITLE')->first();
            if($styleTitle === null){
                Setting::create(['key' => 'STYLE_BIO_TITLE', 'value' => $request->style_bio_title]);
            }else{
                $styleTitle->value = $request->style_bio_title;
                $styleTitle->save();
            }
        }
        if($request->main_photo !== null){
            $appLogo = Setting::where('key', 'MAIN_PHOTO')->first();

            if($appLogo === null){
                Storage::disk('public_root')->putFileAs('files/img/main', $request->file('main_photo'), 'main.'. $request->file('main_photo')->extension());
                Setting::create([
                    'key' => 'MAIN_PHOTO',
                    'value' => 'main.'.$request->file('main_photo')->extension(),
                ]);
            }else{
                Storage::disk('public_root')->delete('files/img/main/'. $appLogo->value);
                Storage::disk('public_root')->putFileAs('files/img/main', $request->file('main_photo'), 'main.'. $request->file('main_photo')->extension());
                $appLogo->value = 'main.'.$request->file('main_photo')->extension();
                $appLogo->save();
            }
        }

        Session::flash('message', 'Nastavení vzhledu bylo uloženo');
        return redirect()->route('settings');
    }

    public function storeTerms(Request $request){
        $this->validate($request, [
            'site_terms' => 'required|string',
        ]);

        Setting::create([
            'key' => 'SITE_TERMS',
            'value' => $request->site_terms
        ]);

        Session::flash('message', 'Obchodní podmínky uloženy.');
        return redirect()->route('settings');
    }

    public function writeEnv($values){
        $newEnvString = null;

        $keys = [];
        foreach ($values as $key => $value){
            $newEnvString = $newEnvString."\n".$key."=".$value;

            array_push($keys, $key);
        }

        $env = null;
        if(file_exists(base_path('.env'))){
            $env = file_get_contents(base_path('.env'));
        };

        $envArray = explode("\n", $env);

        foreach ($keys as $key){
            $unsetKey = key(preg_grep('/^'. $key .'/i', $envArray));
            unset($envArray[$unsetKey]);
        }
        $envUnset = implode("\n", $envArray);
        $envNew = $envUnset.$newEnvString;

        try {
            file_put_contents(base_path('.env'), $envNew);

            Artisan::call('config:clear');

            return true;
        } catch (\Exception $e){
            return false;
        }
    }
}
