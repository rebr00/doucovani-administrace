<?php

namespace App\Http\Controllers;

use App\Mail\Message;
use App\Models\Course;
use App\Models\Setting;
use App\Models\Term;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

/**
 * Class EmailController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro poslání emailu vybrané skupině uživatelů
 */

class EmailController extends Controller
{
    public function index(){
        $emailSettings = Setting::where('key', 'like', 'EMAIL%')
            ->get();
        if(count($emailSettings) === 0){
            Session::flash('message', 'Pro poslání emailu je potřeba nastavit SMTP v sekci "Email". Informace ohledně SMTP naleznete u poskytovatelé emailové služby.');
            return redirect()->route('settings');
        }

        $courses = Course::all('id', 'name');
        $coursesTerms = Course::whereHas('terms')
            ->get();

        return view('email', [
            'courses' => $courses,
            'coursesTerms' => $coursesTerms
        ]);
    }

    public function getTerms(Course $course){
        $terms = Course::find($course->id)->terms()
            ->orderBy('date', 'desc')
            ->get();

        return response()->json(['terms' => $terms->toArray()]);
    }

    public function send(Request $request){
        $this->validate($request, [
            'radio' => 'required',
            'title' => 'required|string',
            'text' => 'required|string',
        ]);

        $emails = null;
        switch ($request->radio){
            case "all":
                $emails = User::where('role', 0)
                    ->get('email');
                break;
            case "courses":
                $course = Course::find($request->course);
                if($course->price === null){
                    $emails = User::whereHas( 'terms', function ($x) use ($course){
                        $x->where('course_id', $course->id);
                    })
                        ->where('role', 0)
                        ->get()
                        ->pluck('email');
                }else{
                    $emails = User::whereHas( 'payments', function ($x) use ($course){
                        $x->where('course_id', $course->id)
                            ->where('status', 1);
                    })
                        ->where('role', 0)
                        ->get()
                        ->pluck('email');
                }
                break;
            case "terms":
                $emails = Term::find($request->term)->users()
                    ->get()
                    ->pluck('email');
                break;
        }

        foreach ($emails as $to){
            Mail::to($to)->send(new Message($request->title, $request->text));
        }

        Session::flash('message', 'Email byl odeslán studentům');
        return redirect()->route('email');
    }
}
