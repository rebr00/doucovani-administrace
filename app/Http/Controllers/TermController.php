<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Term;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class TermController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro CRUD termínů kurzu
 */

class TermController extends Controller
{
    public function index(){
        $terms = Term::where('date', '>', Carbon::now())
            ->sortable(['date' => 'asc'])
            ->paginate(10);

        return view('courses.terms', ['terms' => $terms]);
    }

    public function showFinishedTerms(){
        $terms = Term::where('date', '<=', Carbon::now())
            ->sortable(['date' => 'asc'])
            ->paginate(10);

        return view('courses.terms', [
            'terms' => $terms,
            'old' => true,
        ]);
    }

    public function create(Course $course = null){
        $courses = Course::get()
            ->where('lesson',0);

        if(count($courses) === 0){
            Session::flash('message', 'Nelze vytvořit termín, pokud neexistuje žádný kurz!');

            return redirect()->route('courses');
        }

        return view('courses.terms_create', [
            'courses' => $courses,
            'courseDetail' => $course,
        ]);
    }

    public function store(Request $request){
        $this->validate($request,[
            'course' => 'required|numeric',
            'date' => 'required|date|after_or_equal:today',
            'capacity' => 'required|numeric|min:1'
        ]);

        Term::create([
            'course_id' => $request->course,
            'date' => $request->date,
            'capacity' => $request->capacity,
            'address' => $request->address,
            'note' => $request->note,
        ]);

        Session::flash('message', 'Termín byl úspěšně vytvořen');
        return redirect()->route('terms');
    }

    public function show(Term $term){
        $users = User::whereNotIn('id', $term->users->pluck('id'))->get();

        return view('courses.terms_detail', [
            'term' => $term,
            'users' => $users,
        ]);
    }

    public function edit(Term $term){
        $courses = Course::get()
            ->where('lesson',0);

        return view('courses.terms_edit', [
            'term' => $term,
            'courses' => $courses,
        ]);
    }

    public function update(Term $term, Request $request){
        $this->validate($request,[
            'date' => 'required|date|after_or_equal:today',
            'capacity' => 'required|numeric|min:1'
        ]);

        $term->date = $request->date;
        $term->capacity = $request->capacity;
        $term->address = $request->address;
        $term->note = $request->note;
        $term->save();

        Session::flash('message', 'Změny byly uloženy');
        return redirect()->route('terms');
    }

    public function delete(Term $term){
        $term->delete();

        Session::flash('message', 'Termín byl smazán');
        return redirect()->route('terms');
    }

    public function attachUser(Term $term, Request $request){
        $user = User::where('email', $request->email)->first();

        if($user){
            if(!$term->users->contains($user)){
                $term->users()->attach($user);
                Session::flash('message', 'Student přidán k termínu.');
            }else{
                Session::flash('message', 'Tento student je již přihlášen na tento termín.');
            }
        }else{
            Session::flash('message', 'Student s tímto emailem neexistuje.');
        }

        return back();
    }

    public function detachUser(Term $term, User $user){
        $term->users()->detach($user->id);

        Session::flash('message', 'Uživatel byl z termínu odebrán.');

        return back();
    }
}
