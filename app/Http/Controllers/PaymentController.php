<?php

namespace App\Http\Controllers;

use App\Mail\Message;
use App\Models\Payment;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro potvrzení/zamítnutí a smazání plateb
 */

class PaymentController extends Controller
{
    public function index(){
        $payments = Payment::sortable('status')
            ->latest()
            ->paginate(10);

        return view('courses.payments',[
            'payments' => $payments
        ]);
    }

    public function confirmPayment(Request $request){
        $payment = Payment::find($request->payment);
        if($payment !== null){
            $payment->status = 1;
            $payment->save();
        }

        $emailSettings = Setting::where('key', 'like', 'EMAIL%')
            ->get();
        if(count($emailSettings) !== 0){
            $mailText = 'Vaše platba za kurz '. $payment->course->name.' byla potvrzena. Nyní máte ke kurzu přístup.';
            Mail::to($payment->user->email)->send(new Message('Potvrzení platby', $mailText));
        }

        Session::flash('message', 'Platba byla potvrzena');
        return redirect()->route('payments');
    }

    public function denyPayment(Request $request){
        $payment = Payment::find($request->payment);
        if($payment !== null){
            $payment->status = 0;
            $payment->save();
        }

        Session::flash('message', 'Potvrzení platby bylo zrušeno');
        return redirect()->route('payments');
    }

    public function delete(Payment $payment){
        $payment->delete();

        Session::flash('message', 'Platba byla smazána');
        return redirect()->route('payments');
    }
}
