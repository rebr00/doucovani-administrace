<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class CommentController
 * @package App\Http\Controllers
 * @author Ronald Rebernigg
 * Controller pro zobrazení a smazání komentářů
 */

class CommentController extends Controller
{
    public function index(){
        $comments = Comment::sortable()
            ->paginate(10);

        return view('courses.comments', ['comments' => $comments]);
    }

    public function delete(Comment $comment){
        $children = Comment::where('parent_id', $comment->id)->get();
        if($children !== null){
            foreach ($children as $child){
                $child->delete();
            }
        }

        $comment->delete();

        Session::flash('message', 'Komentář byl odstraněn.');
        return redirect()->route('comments');
    }
}
