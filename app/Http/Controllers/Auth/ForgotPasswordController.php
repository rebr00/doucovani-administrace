<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

/**
 * Class ForgotPasswordController
 * @package App\Http\Controllers\Auth
 * @author Ronald Rebernigg
 * Controller pro resetování hesla
 */

class ForgotPasswordController extends Controller
{
    public function index(){
        return view('web.auth.forgot_password');
    }

    public function passwordResetRequest(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $status = Password::sendResetLink($request->only('email'));

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]);
    }

    public function showReset($token){
        return view('web.auth.password_reset', [
            'token' => $token
        ]);
    }

    public function passwordReset(Request $request){
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request){
                $user->forceFill([
                    'password' => Hash::make($password)
                ]);
                $user->save();
                event(new PasswordReset($user));
            }
        );

        return $status == Password::PASSWORD_RESET
            ? redirect()->route('web.login')->with('message', __($status))
            : back()->withErrors(['message' => [__($status)]]);
    }
}
