<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 * @author Ronald Rebernigg
 * Controller pro přihlášení uživatele v administraci a na klientské části
 */

class LoginController extends Controller
{
    function index(){
        return view('auth.login');
    }

    function indexWeb(){
        $emailSettings = Setting::where('key', 'like', 'EMAIL%')
            ->get();
        $hasEmail = false;
        if(count($emailSettings) > 0){
            $hasEmail = true;
        }

        return view('web.auth.login',[
            'email' => $hasEmail,
        ]);
    }

    function login(Request $request){
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ]);

        if(!auth()->attempt($request->only('email', 'password'), $request->remember)){
            return back()->with('status', 'Invalid login credentials');
        }

        return redirect()->route('home');
    }

    function loginWeb(Request $request){
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ]);

        if(!auth()->attempt($request->only('email', 'password'), $request->remember)){
            return back()->with('status', 'Invalid login credentials');
        }

        return redirect()->route('web.home');
    }
}
