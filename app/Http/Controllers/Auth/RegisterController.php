<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * @package App\Http\Controllers\Auth
 * @author Ronald Rebernigg
 * Controller pro registraci uživatele, odděleno pro administraci a klientskou část
 */

class RegisterController extends Controller
{
    public function index(){
        if(config('install.installed') === true){
            return redirect()->route('home');
        }

        return view('auth.register');
    }

    public function indexWeb(){
        return view('web.auth.register');
    }

    public function register(Request $request){
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|confirmed'
        ]);

        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 1
        ]);

        auth()->attempt($request->only('email', 'password'));

        return redirect()->route('home');
    }

    public function registerWeb(Request $request){
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|confirmed'
        ]);

        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 0
        ]);

        auth()->attempt($request->only('email', 'password'));

        return redirect()->route('web.home');
    }
}
