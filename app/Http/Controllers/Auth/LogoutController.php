<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class LogoutController
 * @package App\Http\Controllers\Auth
 * @author Ronald Rebernigg
 * Controller pro odhlášení uživatele
 */

class LogoutController extends Controller
{
    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }

    public function logoutWeb()
    {
        Auth::logout();

        return redirect()->route('web.home');
    }
}
