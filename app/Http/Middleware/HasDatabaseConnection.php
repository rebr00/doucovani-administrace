<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class HasDatabaseConnection
 * @package App\Http\Middleware
 * @author Ronald Rebernigg
 * Middleware, který kontroluje zdali byla aplikace nainstalována
 */

class HasDatabaseConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(config('install.installed') === false){
            return redirect()->route('install');
        }
        return $next($request);
    }
}
