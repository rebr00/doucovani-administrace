<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class IsAdmin
 * @package App\Http\Middleware
 * @author Ronald Rebernigg
 * Middleware pro ošetření přístupu do administrace jen pro administrátora
 */

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check()){
            if( auth()->user()->isAdmin()){
                return $next($request);
            }else{
                Auth::logout();
                Session::flash('message', 'Nemáte administrátorské oprávnění');
                return redirect()->route('login');
            }
        }
        return redirect()->route('login');
    }
}
