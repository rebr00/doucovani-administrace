<?php

namespace App\Http\View\Composers;


use App\Models\Setting;
use App\Models\Site;
use Illuminate\View\View;

/**
 * Class HeaderComposer
 * @package App\Http\View\Composers
 * @author Ronald Rebernigg
 * Composer se provede vždy, když je vykreslen view web.layouts.header, předá pohledu nastavení logo a vytvořené stránky
 */

class HeaderComposer
{
    protected $sites;
    protected $logo;

    public function __construct()
    {
        $this->logo = Setting::where('key', 'APP_LOGO')->first();
        $this->sites = Site::orderBy('title')
            ->get(['id', 'title']);
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('sites', $this->sites)
            ->with('logo', $this->logo);
    }
}
