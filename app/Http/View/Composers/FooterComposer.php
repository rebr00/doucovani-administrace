<?php

namespace App\Http\View\Composers;


use App\Models\Setting;
use App\Models\Site;
use Illuminate\View\View;

/**
 * Class FooterComposer
 * @package App\Http\View\Composers
 * @author Ronald Rebernigg
 * Composer se provede vždy, když je vykreslen view web.layouts.footer, předá pohledu nastavení z databáze
 */

class FooterComposer
{
    protected $sites;
    protected $settings;

    public function __construct()
    {
        $settings = Setting::where('key', 'like', 'CONTACT_%')
            ->get();
        $settingsCollection = collect();

        $appName = Setting::where('key', 'APP_NAME')->first();
        $email = Setting::where('key', 'EMAIL_ADDRESS')->first();
        $terms = Setting::where('key', 'SITE_TERMS')->first();

        foreach ($settings as $setting){
            $settingsCollection->put($setting->key, $setting->value);
        }

        if($appName){
            $settingsCollection->put($appName->key, $appName->value);
        }
        if($email){
            $settingsCollection->put($email->key, $email->value);
        }

        if($terms){
            $settingsCollection->put($terms->key, $terms->value);
        }
        $this->settings = $settingsCollection;

        $this->sites = Site::orderBy('title')
            ->get(['id', 'title']);
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'settings' => $this->settings,
            'sites' => $this->sites,
        ]);
    }
}
