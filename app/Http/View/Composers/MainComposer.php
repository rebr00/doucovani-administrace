<?php

namespace App\Http\View\Composers;


use App\Models\Setting;
use App\Models\Site;
use Illuminate\View\View;

/**
 * Class MainComposer
 * @package App\Http\View\Composers
 * @author Ronald Rebernigg
 * Composer se provede vždy, když je vykreslen view web.layouts.main, pohledu předá obrázek na pozadí nadpisu pohledu
 */

class MainComposer
{
    protected $background;

    public function __construct()
    {
        $this->background = Setting::where('key', 'MAIN_PHOTO')->first();
    }

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('background', $this->background);
    }
}
