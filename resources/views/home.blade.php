@extends('layouts.main', ['title' => 'Přehled'])

@section('content')
    <div class="col-xl-10 col-xxl-10 d-flex">
        <div class="w-100">
            <div class="row">
                <div class="col-12 col-lg-12 col-xxl-12 d-flex">
                    @if(count($payments) > 0)
                        <div class="card flex-fill">
                            <div class="card-header">
                                <h5 class="card-title mb-0">Nejnovější platby</h5>
                            </div>
                            <table class="table table-hover my-0">
                                <thead>
                                <tr>
                                    <th>Kurz</th>
                                    <th class="d-none d-xl-table-cell">Datum vytvoření platby</th>
                                    <th class="d-none d-xl-table-cell">Student</th>
                                    <th class="d-none d-xl-table-cell">Variabilní symbol</th>
                                    <th>Stav</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->course->name ?? 'Odstraněný kurz' }}</td>
                                        <td class="d-none d-xl-table-cell">{{ \Carbon\Carbon::parse($payment->created_at)->format('d.m.Y H:i') }}</td>
                                        <td class="d-none d-xl-table-cell">
                                            @if($payment->user) {{ $payment->user->first_name.' '. \Illuminate\Support\Str::limit($payment->user->last_name, 1, ".") }} @else Odstraněný uživatel @endif
                                        </td>
                                        <td>{{ $payment->reference_number }}</td>
                                        <td>
                                            @if($payment->status === 1)
                                                <span class="badge bg-success">Zaplaceno</span>
                                            @else
                                                <span class="badge bg-danger">Nezaplaceno</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card border-0">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Počet nově registrovaných uživatelů</h5>
                            <h1 class="mt-1 mb-3">{{ $users }}</h1>
                            <div class="mb-1">
                                <span class="text-muted">Za aktuální měsíc</span>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Počet prodaných kurzů</h5>
                            <h1 class="mt-1 mb-3">{{ $paymentsMonth }}</h1>
                            <div class="mb-1">
                                <span class="text-muted">Za aktuální měsíc</span>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Zisk</h5>
                            <h1 class="mt-1 mb-3">{{ $fmt->formatCurrency($financeMonth, 'CZK') }}</h1>
                            <div class="mb-1">
                                <span class="text-muted">Za aktuální měsíc</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card border-0">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Počet nově registrovaných uživatelů</h5>
                            <h1 class="mt-1 mb-3">{{ $usersTotal }}</h1>
                            <div class="mb-1">
                                <span class="text-muted">Celkem</span>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Počet prodaných kurzů</h5>
                            <h1 class="mt-1 mb-3">{{ $paymentsYear }}</h1>
                            <div class="mb-1">
                                <span class="text-muted">Za aktuální rok</span>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Zisk</h5>
                            <h1 class="mt-1 mb-3">{{ $fmt->formatCurrency($financeYear, 'CZK') }}</h1>
                            <div class="mb-1">
                                <span class="text-muted">Za aktuální rok</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#homeLink').addClass('active');
    </script>
@endsection

