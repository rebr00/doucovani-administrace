<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin dashboard">
    <meta name="author" content="Ronald Rebernigg">

    <title>{{ $title }}</title>

    <link href="{{ asset("css/theme.css") }}" rel="stylesheet">
    <link href="{{ asset("css/app.css") }}" rel="stylesheet">
</head>
<body style="background-color: #222e3c !important">
    <div class="container d-flex">
        <div class="row justify-content-center align-self-center">
            <div class="col-10 align-middle">
                <div class="card my-5">
                    <div class="card-body">
                        <h5 class="card-title" style="font-size: 1.8rem !important;">{{ $title }}</h5>
                        <div class="row justify-content-center">
                            <div class="col-11">
                                {{ $slot }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
