<script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
        selector: '.wysiwyg',
        language: 'cs',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        menubar: 'file edit view insert format tools table help',
        toolbar: 'bold italic underline | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist | forecolor backcolor removeformat | insertfile image media link codesample | charmap emoticons | fullscreen preview print',
        height: 400,
        file_picker_types: 'file, image, media',
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        images_upload_handler: function (blobInfo, success, failure) {
            let data = new FormData();
            data.append('file', blobInfo.blob(), blobInfo.filename());
            console.log(blobInfo.filename());
            axios.post('/upload/wysiwyg', data)
                .then(function (res) {
                    success(res.data.location);
                })
                .catch(function (err) {
                    failure('HTTP Error: ' + err.message);
                });
        },
    });
</script>
