<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Admin dashboard">
    <meta name="author" content="Ronald Rebernigg">

    <title>{{ $title }}</title>

    <link href="{{ asset("css/theme.css") }}" rel="stylesheet">
    <link href="{{ asset("css/app.css") }}" rel="stylesheet">
</head>
<body style="background-color: #222e3c !important">
    {{ $slot }}
</body>
</html>
