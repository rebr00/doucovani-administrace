@extends('layouts.main', ['title' => 'Novinky'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12">
            <a href="{{ route('news.create') }}" class="btn btn-primary float-end">+ Přidat příspěvek</a>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('title', 'Titulek')</th>
                                <th scope="col">@sortablelink('user.last_name', 'Autor')</th>
                                <th scope="col">@sortablelink('created_at', 'Datum přidání')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($articles->count() < 1)
                                    <tr class="text-center">
                                        <td colspan="4">Dosud nebyla vytvořena žádná novinka</td>
                                    </tr>
                                @endif
                                @foreach($articles as $article)
                                    <tr>
                                        <td scope="row">{{ $article->title }}</td>
                                        <td>{{ $article->user->first_name }} {{ $article->user->last_name }}</td>
                                        <td>{{ $article->created_at->format('d.m.Y H:i') }}</td>
                                        <td class="text-right">
                                            <a title="Zobrazit na webu" href="{{ route('web.articles.detail', $article) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                            <a title="Edit" href="{{ route('news.edit', $article) }}" class="p-2"> <i class="bi bi-pencil-square"></i></a>
                                            <form class="d-inline" action="{{ route('news.delete', $article) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $articles->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#newsLink').addClass('active');
    </script>
@endsection
