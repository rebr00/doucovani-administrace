@extends('layouts.main', ['title' => 'Editace příspěvku'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('news.edit', $article->id) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                <label for="title" class="form-label">Titulek*</label>
                                <input type="text" class="form-control" name="title" value="{{ $article->title }}">
                                @error('title')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="body" class="form-label">Obsah*</label>
                                <textarea class="form-control wysiwyg" name="body">
                                    {{ $article->body }}
                                </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a href="{{ route('news') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
            selector: '.wysiwyg',
            plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            menubar: 'file edit view insert format tools table help',
            toolbar: 'bold italic underline | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | bullist | forecolor backcolor removeformat | charmap emoticons | fullscreen preview print | image media link codesample',
        });
    </script>
@endsection

