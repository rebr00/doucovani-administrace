@extends('layouts.main', ['title' => 'Nový příspěvek'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('news.create') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                <label for="title" class="form-label">Titulek<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                                @error('title')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="body" class="form-label">Obsah<span class="text-danger">*</span></label>
                                <textarea class="form-control wysiwyg" name="body">
                                    {{ old('body') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <a href="{{ route('news') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <x-wysiwyg></x-wysiwyg>
@endsection

