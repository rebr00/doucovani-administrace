<x-auth title="Registration">
    <div class="d-flex justify-content-center mt-7">
        <div class="w-50 rounded bg-white p-5">
            <h2>Registration</h2>
            <form action="{{ route('register') }}" method="post">
                @csrf


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</x-auth>
