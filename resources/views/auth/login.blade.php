<x-auth title="Login">
    <div class="d-flex justify-content-center mt-7">
        <div class="w-50 rounded bg-white p-5">
            <h2>Login</h2>

            @if(Session::has('message'))
                <div class="alert alert-danger">{{ Session::get('message') }}</div>
            @endif

            <form action="{{ route('login') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">

                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Heslo</label>
                    <input type="password" class="form-control" id="password" name="password">

                    @error('password')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" name="remember" id="remember">
                    <label class="form-check-label" for="remember">Zapamatuj si přihlášení</label>
                </div>

                @if( session('status') )
                    <div class="text-danger">{{ session('status') }}</div>
                @endif

                <button type="submit" class="btn btn-primary">Přihlásit se</button>
            </form>
        </div>
    </div>
</x-auth>
