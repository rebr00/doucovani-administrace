@extends('layouts.main', ['title' => 'Napsat email'])

@section('content')

    @if(Illuminate\Support\Facades\Session::has('message'))
        <div id="messageAlert" class="alert alert-info m-2">
            {{ Illuminate\Support\Facades\Session::get('message') }}
        </div>
    @endif
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-check">
                                <input name="radio" id="radioAll" class="form-check-input" type="radio" checked value="all">
                                <label for="radioAll" class="form-check-label">Poslat všem</label>
                            </div>
                            <div class="form-check">
                                <input name="radio" id="radioCourses" class="form-check-input" type="radio" value="courses">
                                <label for="radioCourses" class="form-check-label">Poslat studentům z kurzu</label>
                            </div>
                            <div class="form-check">
                                <input name="radio" id="radioTerms" class="form-check-input" type="radio" value="terms">
                                <label for="radioTerms" class="form-check-label">Poslat studentům z termínu</label>
                            </div>

                            <div class="my-3 courses">
                                <label for="course" class="form-label">Kurz<span class="text-danger">*</span></label>
                                <select name="course" class="form-control" aria-label="Vybrání kurzu">
                                    <option selected hidden value="null">Vyberte kurz</option>
                                    @foreach($courses as $course)
                                        <option class="course" value="{{ $course->id }}">{{ $course->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="my-3 coursesTerms">
                                <label for="courseTerms" class="form-label">Kurz<span class="text-danger">*</span></label>
                                <select name="courseTerms" class="form-control" aria-label="Vybrání kurzu">
                                    <option selected hidden value="null">Vyberte kurz</option>
                                    @foreach($coursesTerms as $course)
                                        <option class="course" value="{{ $course->id }}">{{ $course->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3 terms">
                                <label for="term" class="form-label">Termín<span class="text-danger">*</span></label>
                                <select name="term" class="form-control" aria-label="Vybrání termínu kurzu">
                                    <option selected hidden>Vyberte termín</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="title" class="form-label">Předmět<span class="text-danger">*</span></label>
                                <input placeholder="Zde napiště předmět emailu" type="text" value="{{ old('title') }}" class="form-control" name="title">
                                @error('title')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="text" class="form-label">Email<span class="text-danger">*</span></label>
                                <textarea class="form-control wysiwyg" placeholder="Zde napiště obsah emailu" name="text">{{ old('text') }}</textarea>
                                @error('text')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success"><i class="bi bi-envelope-fill"></i> Poslat email</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <x-wysiwyg></x-wysiwyg>
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#emailLink').addClass('active');

        let coursesSelect = $('.courses');
        let coursesTermsSelect = $('.coursesTerms');
        let termsSelect = $('.terms');
        let selectedCourse;

        $('select[name="courseTerms"]').change(function (){
            selectedCourse =  $(this).children('option:selected').val();

            getTerms(selectedCourse);
        });

        coursesSelect.hide();
        coursesTermsSelect.hide();
        termsSelect.hide();

        $('input[type=radio]').change(function (){
            switch ($(this).val()){
                case 'all':
                    $('.courses').hide().prop('disabled', true);
                    $('.coursesTerms').hide().prop('disabled', true);
                    $('.terms').hide().prop('disabled', true);
                    break;
                case 'courses':
                    $('.courses').show().prop('disabled', false);
                    $('.coursesTerms').hide().prop('disabled', true);
                    $('.terms').hide().prop('disabled', true);
                    break;
                case 'terms':
                    getTerms(selectedCourse);
                    $('.courses').hide().prop('disabled', true);
                    $('.coursesTerms').show().prop('disabled', false);
                    $('.terms').show().prop('disabled', false);
                    break;
            }
        });

        function getTerms(courseId){
            $.ajax({
                type:'GET',
                url: window.location.href+'/'+courseId+'/terms',
                success:function(data) {
                    $('select[name="term"]').html('');
                    $(data.terms).each(function (key, value){
                        var date = new Date(value.date).toLocaleString();
                        let dateString = date.slice(0, date.length-3);
                        $('select[name="term"]').append('<option value="'+ value.id +'">'+ dateString +'</option>');
                    });
                },
                error: function (error){
                    $('select[name="term"]').html('<option disabled selected>Nejdříve prosím vyberte kurz</option>')
                    console.log("errorDD", error);
                }
            });
        }
    </script>
@endsection
