@extends('web.layouts.main', ['title' => 'Platba'])

@section('content')
    <section>
        <div class="row justify-content-center p-3">
            <img src="{{ $qr->getQRCodeImage(false, 200) }}" style="max-width: 400px">
        </div>
        <div class="row justify-content-center">
            <div class="col-6">
                <table class="table">
                    <tr>
                        <td>Stav</td>
                        @if($payment->status === 1)
                            <td><span class="badge bg-success">Zaplaceno</span></td>
                        @else
                            <td><span class="badge bg-danger">Nezaplaceno</span></td>
                        @endif
                    </tr>
                    <tr>
                        <td class="fw-bold">
                            Číslo účtu
                        </td>
                        <td>
                            {{ $account->value }}
                        </td>
                    </tr>
                    <tr>
                        <td class="fw-bold">
                            Částka
                        </td>
                        <td>
                            {{ $payment->amount }} Kč
                        </td>
                    </tr>
                    <tr>
                        <td class="fw-bold">
                            Variabilní symbol
                        </td>
                        <td>
                            {{ $reference_number }}
                        </td>
                    </tr>
                    <tr>
                        <td class="fw-bold">
                            Poznámka
                        </td>
                        <td>
                            Platba kurzu: {{ $course->name ?? 'Odstraněný kurz' }}, {{ \Illuminate\Support\Facades\Auth::user()->first_name." ".\Illuminate\Support\Facades\Auth::user()->last_name }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
@endsection
