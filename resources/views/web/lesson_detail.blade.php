@extends('web.layouts.main', ['title' => $lesson->order.". ".$lesson->title])

@section('content')
    @if(Illuminate\Support\Facades\Session::has('message'))
        <div class="row justify-content-center">
            <div class="col-6">
                <div id="messageAlert" class="alert alert-success m-2">
                    {{ Illuminate\Support\Facades\Session::get('message') }}
                </div>
            </div>
        </div>
    @endif
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 bg-light p-3 rounded">
                    <div class="row justify-content-center">
                        <div class="col-12 p-2">
                            {!! $lesson->body !!}
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-6 text-left">
                            @if($previous)
                                <a href="{{ route('web.lesson.detail', $previous) }}" class="btn btn-secondary"><i class="bi bi-arrow-left"></i> Předchozí lekce</a>
                            @endif
                        </div>
                        <div class="col-6 text-right">
                            @if($next)
                                <a href="{{ route('web.lesson.detail', $next) }}" class="btn btn-secondary">Následující lekce <i class="bi bi-arrow-right"></i></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-1">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-6">
                    <h4 class="my-3">Diskuze</h4>
                </div>
                @if(\Illuminate\Support\Facades\Auth::check())
                    <div class="col-6 text-right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#commentModal" role="button">Přidat komentář</button>
                    </div>
                @endif
            </div>
            @if(count($lesson->comments) === 0)
                <div class="row justify-content-center">
                    <div class="col-12"><p>Žádné příspěvky v diskuzi.</p></div>
                </div>
            @endif

            @foreach($comments as $comment)
                <div class="row justify-content-center">
                    <div class="col-12 my-2">
                        <div class="card p-1 bg-light">
                            <div class="row">
                                <div class="col-6 h4">{{ $comment->user->first_name." ".\Illuminate\Support\Str::limit($comment->user->last_name, 1, '.')  }}</div>
                                <div class="col-6 text-right">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <button class="mx-2 p-0 btn btn-link reply text-success" onclick="getComment({{ $comment->id }})" data-toggle="modal" data-target="#commentModal">Odpovědět</button>
                                    @endif
                                    @if(\Illuminate\Support\Facades\Auth::id() === $comment->user->id || \Illuminate\Support\Facades\Auth::user()->isAdmin())
                                        <form action="{{ route('web.lessons.comment.delete', ['lesson' => $lesson, 'comment' => $comment]) }}" method="post" class="d-inline">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-link text-danger p-0" type="submit">Smazat</button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span style="font-size: 0.8rem" class="text-secondary">{{ \Carbon\Carbon::parse($comment->created_at)->format('d.m.Y H:i') }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p style="font-size: 0.9rem">
                                        {{ $comment->body  }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- START 2. level comments -->
                @if($comment->getReplies() !== null)
                    @foreach($comment->getReplies() as $reply)
                        <div class="row justify-content-end my-1">
                            <div class="col-11">
                                <div class="card p-1 bg-light">
                                    <div class="row mb-1">
                                        <div class="col-12 text-right">
                                            @if(\Illuminate\Support\Facades\Auth::id() === $reply->user->id || \Illuminate\Support\Facades\Auth::user()->isAdmin())
                                                <form action="{{ route('web.lessons.comment.delete', ['lesson' => $lesson, 'comment' => $reply]) }}" method="post" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-link text-danger p-0" type="submit">Smazat</button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5 h4">{{ $reply->user->first_name." ".\Illuminate\Support\Str::limit($reply->user->last_name, 1, '.')  }}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <span style="font-size: 0.8rem" class="text-secondary">{{ \Carbon\Carbon::parse($reply->created_at)->format('d.m.Y H:i') }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p style="font-size: 0.9rem">
                                                {{ $reply->body  }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <!-- END 2. level comments -->

            @endforeach

            <div class="row">
                <div class="col-12">
                    {{ $comments->links() }}
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{ route('web.lessons.comment.create', $lesson) }}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="commentModalLabel">Přidat komentář</h5>
                        <button type="button" class="btn p-0" data-dismiss="modal" aria-label="Close"><i style="font-size: 1.5rem" class="bi bi-x"></i></button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="comment_parrent" hidden id="comment-parent-id" value="">
                        <div class="mb-3">
                            <label class="form-label" for="reply">Komentář</label>
                            <textarea class="form-control" name="comment"></textarea>
                            @error('comment')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Zavřít</button>
                        <button type="submit" class="btn btn-primary">Uložit odpověd</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>

    <script>
        function getComment(id){
            let commentId = id;
            $('#comment-parent-id').val(commentId);
        }
    </script>
@endsection
