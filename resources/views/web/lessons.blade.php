@extends('web.layouts.main', ['title' => $course->name.' - Lekce'])

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 text-center my-3">
            <a href="{{ route('web.courses.detail', $course) }}" class="p-3 course-tab">Informace</a>
            @if($course->lesson)
                @if($course->price !== null || $course->price !== 0)
                    @if(\Illuminate\Support\Facades\Auth::check())
                        @if(\Illuminate\Support\Facades\Auth::user()->hasPaidCourse($course))
                            <a href="{{ route('web.lesson.detail', $course->lessons->sortBy('order')->first()) }}" class="p-3">Lekce</a>
                        @else
                            <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                        @endif
                    @else
                        <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                    @endif
                @else
                    <a href="{{ route('web.lesson.detail', $course->lessons->sortBy('order')->first()) }}" class="p-3">Lekce</a>
                @endif
            @endif
            <a href="{{ route('web.ratings', $course) }}" class="p-3">Hodnocení</a>
        </div>
    </div>

    <section class="pt-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <ul class="list-unstyled text-center">
                        <h3>Seznam lekcí v tomto kurzu:</h3>
                        @foreach($course->lessons->sortBy('order') as $lesson)
                            <li><h4>{{ $lesson->order.". ".$lesson->title }}</h4></li>
                        @endforeach
                    </ul>
                </div>
                <span class="text-secondary">Pro zpřístupnění si prosím zakupte kurz.</span>
            </div>
        </div>
    </section>
@endsection
