@extends('web.layouts.main', ['title' => 'Editace účtu'])

@section('content')
    <section>
        <div class="col-12 justify-content-center">
            <div class="col-6 mx-auto">
                <div>
                    <form action="{{ route('web.user.edit') }}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="first_name" class="form-label">Jméno <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $user->first_name }}">
                            @error('first_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="last_name" class="form-label">Přijmení <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user->last_name }}">
                            @error('last_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">Telefon</label>
                            <input type="number" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                            @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="old_password" class="form-label">Staré heslo</label>
                            <input type="password" class="form-control" name="old_password" >
                            @if(\Illuminate\Support\Facades\Session::has('message'))
                                <span class="text-danger">{{ \Illuminate\Support\Facades\Session::get('message') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Nové heslo</label>
                            <input type="password" class="form-control" name="password" >
                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Nové heslo znovu</label>
                            <input type="password" class="form-control" name="password_confirmation" >
                            @error('password_confirmation')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="row justify-content-between p-3 my-3">
                            <div>
                                <a href="{{ route('web.user') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="d-inline-block">
                                <button class="btn btn-primary" type="submit">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
