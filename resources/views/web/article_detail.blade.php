@extends('web.layouts.main', ['title' => $article->title])

@section('content')
    <section class="pt-1">
        <div class="container my-4">
            <div class="row my-4 justify-content-between">
                <div class="col-2">
                    <a class="btn btn-primary" href="{{ route('web.articles') }}">Zpět</a>
                </div>
                @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAdmin())
                    <div class="col-4 text-right">
                        <a class="btn mx-1 btn-primary" href="{{ route('news.edit', $article) }}"><i class="bi bi-pen"></i> Editovat</a>

                        <!-- Share on Facebook link -->
                        <a class="btn text-white" target="popup" style="background-color: #4267B2;"
                           href="https://www.facebook.com/sharer/sharer.php?u={{route('web.articles.detail', $article)}}"
                           onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{route('web.articles.detail', $article)}}','popup','width=600,height=600'); return false;">
                            <i class="bi bi-facebook"></i> Sdílet
                        </a>
                    </div>
                @endif
            </div>
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="justify-content-center">
                        <span class="text-secondary">{{ \Carbon\Carbon::parse($article->created_at)->format('d.m.Y H:i')  }}</span>
                        {!! $article->body !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
