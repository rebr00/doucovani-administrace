@extends('web.layouts.main', ['title' => $site->title])

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="justify-content-center m-5">
                    {!! $site->body !!}
                </div>
            </div>
        </div>
    </section>
@endsection
