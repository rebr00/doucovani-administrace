@extends('web.layouts.main', ['title' => 'Přihlášení'])

@section('content')
    <section>
        <div class="col-12 justify-content-center">
            <div class="col-6 mx-auto">
                <div>
                    <h2>Přihlášení</h2>
                    @if(Session::has('message'))
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif

                    <form action="{{ route('web.login') }}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">

                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Heslo</label>
                            <input type="password" class="form-control" id="password" name="password">

                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" name="remember" id="remember">
                            <label class="form-check-label" for="remember">Zapamatuj si přihlášení</label>
                        </div>

                        @if( session('status') )
                            <div class="text-danger">{{ session('status') }}</div>
                        @endif

                        <div class="row justify-content-between">
                            <div>
                                <button type="submit" class="btn btn-primary">Přihlásit se</button>
                            </div>
                            <div class="d-inline-block">
                                <a class="text-info" href="{{ route('web.register') }}">Registrovat se</a>
                                @if($email)
                                    <a class="text-danger" href="{{ route('password.email') }}">Zapomenuté heslo</a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
