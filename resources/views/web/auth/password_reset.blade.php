@extends('web.layouts.main', ['title' => 'Změna hesla'])

@section('content')
    <section>
        <div class="col-12 justify-content-center">
            <div class="col-6 mx-auto">
                <div>
                    <h2>Obnovení hesla</h2>
                    @if(Session::has('status'))
                        <div class="alert alert-info">{{ Session::get('status') }}</div>
                    @endif
                    <form action="{{ route('password.update') }}" method="post">
                        @csrf
                        <input type="text" class="form-control d-none" name="token" value="{{ $token }}">
                        <div class="mb-3">
                            <label for="email" class="form-label">Zadejte email vašeho účtu<span class="text-danger">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Nové heslo<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password">
                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Potvrdit nové heslo<span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                        <div class="row justify-content-start">
                            <div>
                                <button type="submit" class="btn btn-primary">Odeslat</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
