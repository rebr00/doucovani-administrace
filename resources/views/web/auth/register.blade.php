@extends('web.layouts.main', ['title' => 'Registrace'])

@section('content')
    <section>
        <div class="col-12">
            <div class="col-6 mx-auto">
                <div>
                    <h2>Registrace</h2>
                    <form action="{{ route('web.register') }}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="first_name" class="form-label">Jméno</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}">

                            @error('first_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="last_name" class="form-label">Přijmení</label>
                            <input type="last_name" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}">

                            @error('last_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">

                            @error('email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Heslo</label>
                            <input type="password" class="form-control" id="password" name="password">

                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Potvrdit heslo</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">

                            @error('password_confirmation')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="row justify-content-between">
                            <div>
                                <button type="submit" class="btn btn-primary">Registrovat</button>
                            </div>
                            <div class="d-inline-block">
                                <a href="{{ route('web.login') }}">Již máte vytvořený uživatelský účet? Přihlásit se</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
