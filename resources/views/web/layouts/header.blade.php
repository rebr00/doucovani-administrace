<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="keywords" content="doučování,kurzy,lekce,vzdělání">
    <meta name="description" content="Doučování client side">
    <meta name="author" content="Ronald Rebernigg">

    <title>{{ $title ?? '' }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- Font Awesome icons (free version)-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />

    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset('css/web/styles.css') }}" rel="stylesheet" />

    <!-- Custom stylesheet -->
    <link href="{{ asset('css/web/style.css') }}" rel="stylesheet">

</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        @if($logo !== null)
            <a class="navbar-brand js-scroll-trigger" href="{{ route('web.home') }}"><img src="{{ asset('files/img/logo/'.$logo->value) }}" height="500"/></a>
        @endif
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="bi bi-list ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" id="homeLink" href="{{ route('web.home') }}">Domů</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" id="courseLink" href="{{ route('web.courses') }}">Kurzy</a></li>
                @if($sites)
                    @foreach($sites as $page)
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" id="{{ str_replace(' ', '', strtolower($page->title)) }}Link" href="{{ route('web.site', $page) }}">{{ $page->title }}</a></li>
                    @endforeach
                @endif

                @if(\Illuminate\Support\Facades\Auth::check())
                    <li class="nav-item dropdown text-capitalize">
                        <a class="nav-link dropdown-toggle show" href="#"  data-toggle="dropdown" >
                            <i class="bi bi-person-fill"></i> {{ \Illuminate\Support\Facades\Auth::user()->first_name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
                                <a class="dropdown-item btn btn-link" href="{{ route('home') }}"><i class="bi bi-terminal"></i> Administrace</a>
                            @endif
                            <a class="dropdown-item btn btn-link" href="{{ route('web.user') }}"><i class="bi bi-person-circle"></i> Můj profil</a>
                            <form action="{{ route('web.logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item btn btn-link" type="submit"><i class="bi bi-box-arrow-right align-middle mr-1"></i> Odhlásit se</button>
                            </form>
                        </div>
                    </li>
                @else
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.login') }}">Přihlásit se</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.register') }}">Registrovat se</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
