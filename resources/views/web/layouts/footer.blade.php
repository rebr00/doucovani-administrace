<!-- Footer-->
<footer class="footer bg-light py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-left">
                <ul class="list-unstyled text-small contact">
                    {!! $settings->get('CONTACT_PERSON') ? '<li><i class="bi bi-person-badge-fill"></i> '. $settings->get('CONTACT_PERSON') .'</li>' : '' !!}
                    {!! $settings->get('EMAIL_ADDRESS') ? '<li><i class="bi bi-envelope-fill"></i> '. $settings->get('EMAIL_ADDRESS') .'</li>' : '' !!}
                    {!! $settings->get('CONTACT_PHONE') ? '<li><i class="bi bi-phone-fill"></i> '. $settings->get('CONTACT_PHONE') .'</li>' : '' !!}
                    {!! $settings->get('CONTACT_ADDRESS') ? '<li><i class="bi bi-geo-alt-fill"></i> '. $settings->get('CONTACT_ADDRESS') .'</li>' : '' !!}
                </ul>
            </div>
            <div class="col-lg-4 my-3 my-lg-0">
                {!! $settings->get('CONTACT_FACEBOOK') ? '<a class="btn btn-dark btn-social mx-2" target="_blank" href="'. $settings->get('CONTACT_FACEBOOK') .'"><i class="bi bi-facebook"></i></a>' : '' !!}
                {!! $settings->get('CONTACT_INSTAGRAM') ? '<a class="btn btn-dark btn-social mx-2" target="_blank" href="'. $settings->get('CONTACT_INSTAGRAM') .'"><i class="bi bi-instagram"></i></a>' : '' !!}
                {!! $settings->get('CONTACT_LINKEDIN') ? '<a class="btn btn-dark btn-social mx-2" target="_blank" href="'. $settings->get('CONTACT_LINKEDIN') .'"><i class="bi bi-linkedin"></i></a>' : '' !!}
                {!! $settings->get('CONTACT_YOUTUBE') ? '<a class="btn btn-dark btn-social mx-2" target="_blank" href="'. $settings->get('CONTACT_YOUTUBE') .'"><i class="bi bi-youtube"></i></a>' : '' !!}
            </div>
            <div class="col-lg-4">
                <ul class="list-unstyled bold">
                    <li><a href="{{ route('web.home') }}">Domů</a></li>
                    <li><a href="{{ route('web.courses') }}">Kurzy</a></li>
                    @foreach($sites as $site)
                        <li><a href="{{ route('web.site', $site) }}">{{ $site->title }}</a></li>
                    @endforeach

                    @if($settings->get('SITE_TERMS'))
                        <li><a href="{{ route('web.site.trade') }}">Obchodní podmínky</a></li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <span>&copy; {{ \Carbon\Carbon::now()->year }} {{ $settings->get('APP_NAME') ?? '' }} </span>
            </div>
        </div>
    </div>
</footer>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('js/web/scripts.js') }}"></script>

@yield('scripts')

</body>
</html>
