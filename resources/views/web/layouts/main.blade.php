@include('web.layouts.header')
<main>
    <section class="content-header p-5"
             @if($background !== null)
                 style="display:table;
                     width:100%;
                     background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url({{ asset('files/img/main/'.$background->value) }});
                     background-position:center center;
                     background-repeat: no-repeat;
                     background-size: cover"
             @endif
    >
        <div class="container">
            <h2 class="header-title d-inline-block">{{ $title ?? '' }}</h2>
        </div>
    </section>
    @yield('content')
</main>
@include('web.layouts.footer')
