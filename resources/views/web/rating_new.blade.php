@extends('web.layouts.main', ['title' => 'Přidat hodnocení'])

@section('content')
    <div class="row justify-content-center my-5">
        <div class="col-12">
            <div class="col-6 mx-auto">
                <form method="post" action="{{ route('web.rating.new', $course) }}">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label" for="score">Hodnocení</label>
                        <div class="row justify-content-start">
                            <div id="rateYo"></div>
                            <div class="bold align-self-center" id="ratingText"></div>
                        </div>
                        <input class="form-control d-none" name="score" type="number" value="{{ old('score') }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="text">Slovní hodnocení</label>
                        <textarea class="form-control" name="text">{{ old('text') }}</textarea>
                        @error('text')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Odeslat</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script>
        $(document).ready(function (){
            $('#rateYo').rateYo({
                minValue: 1,
                rating: 1,
                fullStar: true,
                onInit: function (rating, rateYo) {
                    $('#ratingText').text(rating);
                },
                onChange: function (rating, rateYo) {
                    $('#ratingText').text(rating);
                }
            });

            $('#rateYo').click(function () {
                let rating = $('#rateYo').rateYo('rating')
                $('input[name="score"]').val(rating);
            })
        });
    </script>
@endsection
