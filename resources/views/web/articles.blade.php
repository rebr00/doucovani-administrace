@extends('web.layouts.main', ['title' => 'Novinky'])

@section('content')
    <section class="pt-1">
        <div class="container my-4">
            <div class="row">
                <div class="col-12">
                    @foreach($articles as $article)
                        <div>
                            <h3><a href="{{ route('web.articles.detail', $article) }}">{{ $article->title }}</a></h3>
                            <span class="meta-date">{{ \Carbon\Carbon::parse($article->created_at)->format('d.m.Y H:i')  }}</span>
                            <span class="text-left">
                                {!! \Illuminate\Support\Str::limit($article->body, 200, '...') !!}
                            </span>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                {{ $articles->links() }}
            </div>
        </div>
    </section>
@endsection
