@extends('web.layouts.main', ['title' => $course->name])

@section('content')
    <section class="p-0">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10 text-center my-3">
                    <a href="{{ route('web.courses.detail', $course) }}" class="p-3 course-tab">Informace</a>
                    @if(count($terms) > 0)
                        <a href="{{ route('web.terms', $course) }}" class="p-3">Termíny</a>
                    @endif
                    @if($course->lesson && count($course->lessons) > null)
                        @if($course->price !== null && $course->price !== 0)
                            @if(\Illuminate\Support\Facades\Auth::check())
                                @if(\Illuminate\Support\Facades\Auth::user()->hasPaidCourse($course) || \Illuminate\Support\Facades\Auth::user()->isAdmin())
                                    <a href="{{ route('web.lesson.detail', $course->lessons->sortBy('order')->first()) }}" class="p-3">Lekce</a>
                                @else
                                    <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                                @endif
                            @else
                                <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                            @endif
                        @else
                            <a href="{{ route('web.lesson.detail', $course->lessons->sortBy('order')->first()) }}" class="p-3">Lekce</a>
                        @endif
                    @endif
                    <a href="{{ route('web.ratings', $course) }}" class="p-3">Hodnocení</a>
                </div>
            </div>
            @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAdmin())
                <div class="row">
                    <div class="col-md-8 mx-auto text-right">
                        <a class="btn btn-primary" href="{{ route('courses.edit', $course) }}"><i class="bi bi-pen"></i> Editovat</a>
                        <!-- Share on Facebook link -->
                        <a class="btn text-white" target="popup" style="background-color: #4267B2;"
                           href="https://www.facebook.com/sharer/sharer.php?u={{route('web.courses.detail', $course)}}"
                           onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{route('web.courses.detail', $course)}}','popup','width=600,height=600'); return false;">
                            <i class="bi bi-facebook"></i> Sdílet
                        </a>
                    </div>
                </div>
            @endif
            <div class="row justify-content-center mt-2">
                <div class="col-md-8 mx-auto">
                    @if($course->image !== null)
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <img src="{{ asset('files/img/course/'.$course->image) }}" style="border-radius: 20px; max-height: 250px" class="img-fluid w-100" alt="Course image">
                            </div>
                        </div>
                    @endif
                    <div class="row justify-content-center">
                        <div class="col-10 mx-auto pb-5">
                            @if($course->price !== null)
                                <div class="row justify-content-between py-3">
                                    <div class="col-3">
                                        <span class="h4">Cena kurzu {{ $course->price }} Kč</span>
                                    </div>
                                    @if(\Illuminate\Support\Facades\Auth::check() && !\Illuminate\Support\Facades\Auth::user()->hasPayment($course))
                                        @if(!\Illuminate\Support\Facades\Auth::user()->isAdmin())
                                            <div class="col-2 text-end">
                                                <a href="{{ route('web.payment', $course) }}" class="btn btn-success" type="button">Koupit kurz</a>
                                            </div>
                                        @endif
                                    @elseif(\Illuminate\Support\Facades\Auth::check() && !\Illuminate\Support\Facades\Auth::user()->hasPaidCourse($course))
                                        <div class="col-2 text-end">
                                            <div class="row">
                                                <span class="text-danger">Vaše platba zatím nebyla potvrzena.</span>
                                            </div>
                                            <div class="row">
                                                <a href="{{ route('web.user') }}" >Zobrazit moje platby</a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                            {!! $course->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
