@extends('web.layouts.main', ['title' => 'Obchodní podmínky'])

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="justify-content-center m-5">
                    {!! $trade_terms !!}
                </div>
            </div>
        </div>
    </section>
@endsection
