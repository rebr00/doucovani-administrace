<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="keywords" content="doučování,kurzy,lekce,vzdělání">
    <meta name="description" content="Doučování client side">
    <meta name="author" content="Ronald Rebernigg">

    <title>{{ $settings->get('APP_NAME') }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- Font Awesome icons (free version)-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>

    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />

    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset('css/web/styles.css') }}" rel="stylesheet" />

    <!-- Custom stylesheet -->
    <link href="{{ asset('css/web/style.css') }}" rel="stylesheet">

    <style>
        #mainNav .navbar-brand img {
            height: 50px;
        }

        @media (max-width: 992px) {
            .bio-photo{
                text-align: center;
                margin: 10px 0;
            }
        }

        @media (min-width: 992px) {
            #mainNav.navbar-shrink {
                padding-top: 1rem;
                padding-bottom: 1rem;
                background-color: #ffffff;
            }

            #mainNav .navbar-nav .nav-item .nav-link {
                color: #ffffff;
            }

            #mainNav.navbar-shrink .navbar-nav .nav-item .nav-link {
                color: #000000;
            }

            #mainNav .navbar-brand img {
                height: 80px;
            }

            #mainNav.navbar-shrink .navbar-brand svg, #mainNav.navbar-shrink .navbar-brand img {
                height: 75px;
            }
        }
    </style>

</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        @if($logo)
            <a class="navbar-brand js-scroll-trigger" href="{{ route('web.home') }}"><img src="{{ asset('files/img/logo/'. $logo->value) }}" height="500"/></a>
        @endif
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="bi bi-list ml-1"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.home') }}">Domů</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.courses') }}">Kurzy</a></li>
                @foreach($sites as $site)
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.site', $site) }}">{{ $site->title }}</a></li>
                @endforeach
                @if(\Illuminate\Support\Facades\Auth::check())
                    <li class="nav-item dropdown text-capitalize">
                        <a class="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" >
                            <i class="bi bi-person-fill"></i> {{ \Illuminate\Support\Facades\Auth::user()->first_name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
                                <a class="dropdown-item btn btn-link" href="{{ route('home') }}"><i class="bi bi-terminal"></i> Administrace</a>
                            @endif
                            <a class="dropdown-item btn btn-link" href="{{ route('web.user') }}"><i class="bi bi-person-circle"></i> Můj profil</a>
                            <form action="{{ route('web.logout') }}" method="post">
                                @csrf
                                <button class="dropdown-item btn btn-link" type="submit"><i class="bi bi-box-arrow-right align-middle mr-1"></i> Odhlásit se</button>
                            </form>
                        </div>
                    </li>
                @else
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.login') }}">Přihlásit se</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="{{ route('web.register') }}">Registrovat se</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<!-- Masthead-->
<header class="masthead" @if($settings->get('STYLE_SLIDER')) style="background-image: linear-gradient( rgba(0,0,0,0.5), rgba(0, 0, 0, 0.5) ),url( {{ asset('files/img/slider/'.$settings->get('STYLE_SLIDER')) }} );" @endif>
    @if($settings->get('STYLE_TITLE') || $settings->get('STYLE_TEXT'))
        <div class="container">
            <div class="masthead-heading pt-2 text-uppercase">{{ $settings->get('STYLE_TITLE') ?? '' }}</div>
            <div class="masthead-subheading pb-2">{{ $settings->get('STYLE_TEXT') ?? '' }}</div>
        </div>
    @endif
</header>
<!-- Services-->
<section class="page-section" id="services">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading  text-uppercase">{{ $settings->get('STYLE_ICON_TITLE') ?? '' }}</h2>
        </div>
        <div class="row text-center">
            @if($settings->get('STYLE_ICON_1'))
                <div class="col-md-4">
                            <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fas {{ $settings->get('STYLE_ICON_1') }} fa-stack-1x fa-inverse"></i>
                            </span>
                    <h4 class="my-3">{{ $settings->get('STYLE_ICON_1_TITLE') ?? '' }}</h4>
                    <p class="text-muted">{{ $settings->get('STYLE_ICON_1_TEXT') ?? '' }}</p>
            </div>
            @endif

            @if($settings->get('STYLE_ICON_2'))
                <div class="col-md-4">
                            <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fas {{ $settings->get('STYLE_ICON_2') }} fa-stack-1x fa-inverse"></i>
                            </span>
                    <h4 class="my-3">{{ $settings->get('STYLE_ICON_2_TITLE') ?? '' }}</h4>
                    <p class="text-muted">{{ $settings->get('STYLE_ICON_2_TEXT') ?? '' }}</p>
                </div>
            @endif

            @if($settings->get('STYLE_ICON_3'))
                <div class="col-md-4">
                            <span class="fa-stack fa-4x">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fas {{ $settings->get('STYLE_ICON_3') }} fa-stack-1x fa-inverse"></i>
                            </span>
                    <h4 class="my-3">{{ $settings->get('STYLE_ICON_3_TITLE') ?? '' }}</h4>
                    <p class="text-muted">{{ $settings->get('STYLE_ICON_3_TEXT') ?? '' }}</p>
                </div>
            @endif
        </div>
    </div>
</section>
<!-- BIO section -->
@if($settings->get('STYLE_BIO_TEXT'))
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">{{ $settings->get('STYLE_BIO_TITLE') ?? '' }}</h2>
                <h3 class="section-subheading text-muted"></h3>
            </div>
            <div class="row justify-content-center">
                @if($settings->get('STYLE_BIO_PHOTO'))
                    <div class="col-md-4 align-self-center bio-photo">
                        <img class="avatar rounded" width="200" src="{{ asset('files/img/bio/'. $settings->get('STYLE_BIO_PHOTO')) }}">
                    </div>
                @endif
                <div class="col-md-8 align-self-center">
                    <div class="card">
                        <div class="card-body">
                            <p>{{ $settings->get('STYLE_BIO_TEXT') ?? '' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

<!-- News Section -->
@if(count($articles) > 0)
    <section class="page-section" id="portfolio">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Novinky</h2>
            </div>
            <div class="row">
                <div class="col-12">
                    @foreach($articles as $article)
                        <div class="bg-light p-2 my-4 border">
                            <div class="row">
                                <div class="col-12 h4"><a href="{{ route('web.articles.detail', $article) }}">{{ $article->title }}</a></div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    {!! \Illuminate\Support\Str::limit($article->body, 400, '...') !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-end">
                                    <span class="text-secondary">{{ \Carbon\Carbon::parse($article->created_at)->format('d.m.Y H:i')  }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @if($count > 3)
                <div class="row justify-content-center text-center">
                    <div class="col-12">
                        <a href="{{ route('web.articles') }}" class="btn btn-primary">Zobrazit další</a>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endif
@include('web.layouts.footer')

@section('scripts')
    <script>
    </script>
