@extends('web.layouts.main', ['title' => 'Kurzy'])

@section('content')
    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row">
                        @if(count($courses) < 1)
                            <p>Momentálně nejsou dostupné žádné kurzy.</p>
                        @endif
                        @foreach($courses as $course)
                            <div class="col-md-5 m-4">
                                <div class="row justify-content-center">
                                    <a href="{{ route('web.courses.detail', $course) }}">
                                        <div class="card p-1 w-100 h-100">
                                            @if($course->image !== null)
                                                <img src="{{ asset('files/img/course/'.$course->image) }}" class="card-img-top" height="200" width="300">
                                            @endif
                                            <div class="card-body @if($course->image) p-2 @else p-5 @endif text-center align-self-center">
                                                <h5 class="card-title"><a href="{{ route('web.courses.detail', $course) }}">{{ $course->name }}</a></h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {{ $courses->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $('#courseLink').addClass('active');
    </script>
@endsection
