@extends('web.layouts.main', ['title' => $course->name.' - Hodnocení'])

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 text-center my-3">
            @if(Illuminate\Support\Facades\Session::has('message'))
                <div class="row justify-content-center">
                    <div class="col-6">
                        <div id="messageAlert" class="alert alert-success m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    </div>
                </div>
            @endif

            <a href="{{ route('web.courses.detail', $course) }}" class="p-3 course-tab">Informace</a>
                @if(count($course->terms) > 0)
                    <a href="{{ route('web.terms', $course) }}" class="p-3">Termíny</a>
                @endif
                @if($course->lesson && count($course->lessons) > null)
                    @if($course->price !== null && $course->price !== 0)
                        @if(\Illuminate\Support\Facades\Auth::check())
                            @if(\Illuminate\Support\Facades\Auth::user()->hasPaidCourse($course) || \Illuminate\Support\Facades\Auth::user()->isAdmin())
                                <a href="{{ route('web.lesson.detail', $course->lessons->sortBy('order')->first()) }}" class="p-3">Lekce</a>
                            @else
                                <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                            @endif
                        @else
                            <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                        @endif
                    @else
                        <a href="{{ route('web.lesson.detail', $course->lessons->sortBy('order')->first()) }}" class="p-3">Lekce</a>
                    @endif
                @endif
            <a href="{{ route('web.ratings', $course) }}" class="p-3">Hodnocení</a>
        </div>
    </div>
    <div class="col-12">
        @if(\Illuminate\Support\Facades\Auth::check())
            <div class="row mb-3 justify-content-center">
                <div class="col-10 text-right">
                    <a href="{{ route('web.rating.new', $course) }}" class="btn btn-primary text-white"><i class="bi bi-plus"></i> Přidat hodnocení</a>
                </div>
            </div>
        @endif
        @if(count($ratings) === 0)
            <div class="row text-center p-5">
                <div class="col-12">
                    <p>Kurz momentálně nemá žádné hodnocení.</p>
                </div>
            </div>
        @endif
    </div>

    <div class="my-3">
        @foreach($ratings as $rating)
            <div class="row justify-content-center">
                <div class="col-9 my-2">
                    <div class="card p-2 bg-light">

                        <div class="row">
                            <div class="col-6 h4">
                                {{ $rating->user->first_name." ".\Illuminate\Support\Str::limit($rating->user->last_name, 1, '.')  }}
                                @for($i = 0; $i < $rating->score; $i++)
                                    <span class="bold h5"><i style="color: orange" class="bi bi-star-fill"></i></span>
                                @endfor
                            </div>
                            @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAdmin())
                                <div class="col-6 text-right">
                                    <button class="mx-2 text-success p-0 btn btn-link reply" onclick="getRating({{ $rating->id }})" data-toggle="modal" data-target="#replyModal">Odpovědět</button>
                                    <form action="{{ route('web.rating.delete', ['course' => $course, 'rating' => $rating] ) }}" method="post" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-link text-danger p-0" type="submit">Smazat</button>
                                    </form>
                                </div>
                            @endif

                            @if(\Illuminate\Support\Facades\Auth::id() === $rating->user->id && !\Illuminate\Support\Facades\Auth::user()->isAdmin())
                                <div class="col-6 text-right">
                                    <form action="{{ route('web.rating.delete', ['course' => $course, 'rating' => $rating]) }}" method="post" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-light align-self-center text-danger p-0" type="submit">Smazat</button>
                                    </form>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <span style="font-size: 0.8rem" class="text-secondary">{{ \Carbon\Carbon::parse($rating->created_at)->format('d.m.Y H:i') }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p style="font-size: 0.9rem">
                                    {{ $rating->text  }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($rating->reply !== null)
                <div class="row justify-content-center">
                    <div class="col-9">
                        <div class="row justify-content-end">
                            <div class="col-11">
                                <div class="card p-1 my-1 bg-light">
                                    @if(\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->isAdmin())
                                        <div class="row mb-1">
                                            <div class="col-12 text-right">
                                                <form action="{{ route('web.rating.delete', ['course' => $course, 'rating' => $rating] ) }}" method="post" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-link text-danger p-0" type="submit">Smazat</button>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-5 h4">Odpověd doučujícího</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <span style="font-size: 0.8rem" class="text-secondary">{{ \Carbon\Carbon::parse($rating->reply->created_at)->format('d.m.Y H:i') }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <p style="font-size: 0.9rem">
                                                {{ $rating->reply->body  }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        @endforeach
    </div>

    <div class="row justify-content-center">
        {{ $ratings->links() }}
    </div>

    <div class="modal fade" id="replyModal" tabindex="-1" aria-labelledby="replyModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="{{ route('web.rating.reply', $course) }}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="replyModalLabel">Odpovědět na hodnocení</h5>
                        <button type="button" class="btn p-0" data-dismiss="modal" aria-label="Close"><i style="font-size: 1.5rem" class="bi bi-x"></i></button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="rating" hidden id="rating-id" value="">
                        <div class="mb-3">
                            <label class="form-label" for="reply">Text odpovědi</label>
                            <textarea class="form-control" name="reply"></textarea>
                            @error('reply')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Zavřít</button>
                        <button type="submit" class="btn btn-primary">Uložit odpověd</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>

    <script>
        function getRating(id){
            let ratingId = id;
            $('#rating-id').val(ratingId);
        }
    </script>
@endsection
