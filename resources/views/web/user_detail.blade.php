@extends('web.layouts.main', ['title' => 'Můj profil'])

@section('content')
    <section>
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="row justify-content-between">
                        <div>
                            <h2 class="h2">Vaše údaje</h2>
                        </div>
                        <div>
                            <a href="{{ route('web.user.edit') }}" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Změnit údaje</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <label  class="font-weight-bold mr-2">Jméno a přijmení</label>
                                <span>{{ $user->first_name.' '.$user->last_name }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="font-weight-bold mr-2">Email</label>
                                <span>{{ $user->email }}</span>
                            </td>
                            <td>
                                <label class="font-weight-bold mr-2">Telefon</label>
                                <span>{{ $user->phone ?? "-" }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="font-weight-bold mr-2">Datum a čas registrace</label>
                                <span>{{ $user->created_at->format('d.m.Y H:i') }}</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 my-3">
                    <div class="card flex-fill">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Moje platby</h5>
                        </div>
                        <table class="table table-hover my-0">
                            <thead>
                                <tr>
                                    <th>Kurz</th>
                                    <th class="d-none d-xl-table-cell">Datum vytvoření</th>
                                    <th class="d-none d-xl-table-cell">Částka</th>
                                    <th>Stav</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td><a href="{{ route('web.courses.detail', $payment->course) }}">{{ $payment->course->name }}</a></td>
                                    <td class="d-none d-xl-table-cell">{{ \Carbon\Carbon::parse($payment->created_at)->format('d.m.Y H:i') }}</td>
                                    <td class="d-none d-xl-table-cell">{{ $payment->course->price }} Kč</td>
                                    <td>
                                        @if($payment->status === 1)
                                            <span class="badge bg-success">Zaplaceno</span>
                                        @else
                                            <span class="badge bg-danger">Nezaplaceno</span>
                                        @endif
                                    </td>
                                    <td><a href="{{ route('web.payment.detail', $payment) }}">Zobrazit informace k platbě</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! $payments->links() !!}
                </div>
            </div>

        </div>
    </section>
@endsection
