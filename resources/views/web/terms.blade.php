@extends('web.layouts.main', ['title' => $course->name.' - Termíny'])

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 text-center my-3">
            <a href="{{ route('web.courses.detail', $course) }}" class="p-3 course-tab">Informace</a>
            @if(count($terms) > 0)
                <a href="{{ route('web.terms', $course) }}" class="p-3">Termíny</a>
            @endif
            @if($course->lesson)
                @if($course->price !== null || $course->price !== 0)
                    @if(\Illuminate\Support\Facades\Auth::check())
                        @if(\Illuminate\Support\Facades\Auth::user()->hasPaidCourse($course))
                            <a href="{{ route('web.lesson.detail', $course) }}" class="p-3">Lekce</a>
                        @else
                            <a href="{{ route('web.lessons', $course) }}" class="p-3">Lekce</a>
                        @endif
                    @endif
                @else
                    <a href="{{ route('web.lesson.detail', $course) }}" class="p-3">Lekce</a>
                @endif
            @endif
            <a href="{{ route('web.ratings', $course) }}" class="p-3">Hodnocení</a>
        </div>
    </div>

    <section class="pt-0">
        <div class="row justify-content-center">
            @foreach($terms as $term)
                <div class="col-9 mt-3 mb-4">
                    <div class="card bg-light p-2">
                        <div class="row">
                            <div class="col-8 h4">{{ \Illuminate\Support\Carbon::parse($term->date)->format('d.m.Y H:i') }}</div>
                            <div class="col-4 text-right">
                                @if(count($term->users) < $term->capacity && \Illuminate\Support\Facades\Auth::check() && !\Illuminate\Support\Facades\Auth::user()->isAdmin())
                                    @if($course->price !== null || $course->price > 0)
                                        @if(\Illuminate\Support\Facades\Auth::user()->hasPaidCourse($course))
                                            @if(\Illuminate\Support\Facades\Auth::user()->hasTerm($term))
                                                <form class="d-inline" method="post" action="{{ route('web.course.term.detach', $term) }}">
                                                    @csrf
                                                    <button type="submit" class="mx-3 btn btn-danger">Odhlásit se</button>
                                                </form>
                                            @else
                                                <form class="d-inline" method="post" action="{{ route('web.course.term.attach', $term) }}">
                                                    @csrf
                                                    <button type="submit" class="mx-3 btn btn-primary">Přihlásit se</button>
                                                </form>
                                            @endif
                                        @else
                                            <br><span class="text-danger">Pro přihlášení na termín je potřeba nejprve zaplatit kurz</span>
                                        @endif
                                    @else
                                        @if(\Illuminate\Support\Facades\Auth::user()->hasTerm($term))
                                            <form class="d-inline" method="post" action="{{ route('web.course.term.detach', $term) }}">
                                                @csrf
                                                <button type="submit" class="mx-3 btn btn-danger">Odhlásit se</button>
                                            </form>
                                        @else
                                            <form class="d-inline" method="post" action="{{ route('web.course.term.attach', $term) }}">
                                                @csrf
                                                <button type="submit" class="mx-3 btn btn-primary">Přihlásit se</button>
                                            </form>
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="row justify-content-between p-1">
                            <div class="col-6">
                                @if($term->address)
                                    <i class="bi bi-geo-alt-fill"></i> {{ $term->address }}
                                @endif
                            </div>
                            <div class="col-6 text-right"><span>Kapacita: {{ count($term->users)."/".$term->capacity }}</span></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                {!! $term->note !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection
