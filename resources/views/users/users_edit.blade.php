@extends('layouts.main', ['title' => 'Editace uživatele'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12 text-right">
            <form class="d-inline" action="{{ route('users.delete', [$user->id]) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger"><i class="bi bi-trash mr-1"></i>Odstranit</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form action="{{ route('users.edit', [$user->id]) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col mb-3">
                        <label for="first_name" class="form-label">Jméno<span class="text-danger">*</span></label>
                        <input type="text" class="form-control w-50" id="first_name" name="first_name" value="{{ $user->first_name }}">
                        @error('first_name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <label for="last_name" class="form-label">Přijmení<span class="text-danger">*</span></label>
                        <input type="text" class="form-control w-50" id="last_name" name="last_name" value="{{ $user->last_name }}">
                        @error('last_name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col mb-3">
                        <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                        <input type="email" class="form-control w-50" id="email" name="email" value="{{ $user->email }}">
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <label for="phone" class="form-label">Telefon</label>
                        <input type="number" class="form-control w-50" id="email" name="phone" value="{{ $user->phone }}">
                        @error('phone')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col mb-3">
                        <label for="password" class="form-label">Nové heslo</label>
                        <input type="password" class="form-control w-50" id="password" name="password">
                        @error('password')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <label for="password_confirmation" class="form-label">Potvrdit heslo</label>
                        <input type="password" class="form-control w-50" id="password_confirmation" name="password_confirmation">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 mb-3">
                        <label for="first_name" class="form-label">Role</label>
                        <select class="form-control w-50" id="role" name="role">
                            <option @if($user->role == 0) selected @endif value="0">Uživatel</option>
                            <option @if($user->role == 1) selected @endif value="1">Administrátor</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <a href="{{ route('news') }}" class="btn btn-danger">Zrušit</a>
                    </div>
                    <div class="col text-end">
                        <button type="submit" class="btn btn-success">Uložit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
