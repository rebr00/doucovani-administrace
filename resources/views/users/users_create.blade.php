@extends('layouts.main', ['title' => 'Nový uživatel'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('users.create') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="first_name" class="form-label">Jméno<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control w-50" id="first_name" name="first_name" value="{{ old('first_name') }}">
                                    @error('first_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="last_name" class="form-label">Přijmení<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control w-50" id="last_name" name="last_name" value="{{ old('last_name') }}">
                                    @error('last_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email<span class="text-danger">*</span></label>
                                    <input type="email" class="form-control w-50" id="email" name="email" value="{{ old('email') }}">
                                    @error('email')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="phone" class="form-label">Telefon</label>
                                    <input type="number" maxlength="9" class="form-control w-50" id="phone" name="phone" value="{{ old('phone') }}"
                                    data-toggle="tooltip" data-placement="top" title="Zadejte telefonní číslo o délce 9 čísel">
                                    @error('phone')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <label for="password" class="form-label">Heslo<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control w-50" id="password" name="password">
                                    @error('password')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <div class="mb-3">
                                    <label for="password_confirmation" class="form-label">Potvrdit heslo<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control w-50" id="password_confirmation" name="password_confirmation">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="first_name" class="form-label">Role<span class="text-danger">*</span></label>
                            <select class="form-control w-50" id="role" name="role">
                                <option value="0">Uživatel</option>
                                <option value="1">Administrátor</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success">Vytvořit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

