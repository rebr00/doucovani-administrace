@extends('layouts.main', ['title' => 'Uživatelé'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12">
            <a href="{{ route('users.create') }}" class="btn btn-primary float-end">+ Přidat uživatele</a>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('first_name','Jméno')</th>
                                <th scope="col">@sortablelink('last_name','Příjmení')</th>
                                <th scope="col">@sortablelink('email','Email')</th>
                                <th scope="col">@sortablelink('phone','Telefon')</th>
                                <th scope="col">@sortablelink('created_at','Datum registrace')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td scope="row">{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone ?? '-' }}</td>
                                    <td>{{ $user->created_at->format('d.m.Y') }}</td>
                                    <td class="text-right">
                                        <a title="Show" href="{{ route('users.detail', $user) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                        <a title="Edit" href="{{ route('users.edit', $user) }}" class="p-2"> <i class="bi bi-pencil-square"></i></a>
                                        <form class="d-inline" action="{{ route('users.delete', $user) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $users->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#userLink').addClass('active');
    </script>
@endsection
