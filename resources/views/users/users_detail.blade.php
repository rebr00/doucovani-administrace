@extends('layouts.main', ['title' => 'Detail uživatele'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12 text-right">
            <a href="{{ route('users.edit', [$user->id]) }}"><button class="btn btn-primary"><i class="bi bi-pencil-fill mr-1"></i>Upravit</button></a>
            <form class="d-inline" action="{{ route('users.delete', [$user->id]) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger"><i class="bi bi-trash mr-1"></i>Odstranit</button>
            </form>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <label  class="font-weight-bold mr-2">Jméno a přijmení</label>
                                <span>{{ $user->first_name.' '.$user->last_name }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="font-weight-bold mr-2">Email</label>
                                <span>{{ $user->email }}</span>
                            </td>
                            <td>
                                <label class="font-weight-bold mr-2">Telefon</label>
                                <span>{{ $user->phone ?? "-" }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="font-weight-bold mr-2">Datum a čas registrace</label>
                                <span>{{ $user->created_at->format('d.m.Y H:i') }}</span>
                            </td>
                            <td>
                                <label class="font-weight-bold mr-2">Role</label>
                                <span>@if($user->role == 0) Uživatel @else Administrátor @endif</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-12">
            <div class="accordion mt-3" id="accordionPayments">
                <div class="card">
                    <div class="card-header bg-white" id="accordionPaymentsHeading">
                        <h5 class="font-weight-bold my-2">
                            <a href="#" data-toggle="collapse" data-target="#collapsePayments" aria-expanded="false" aria-controls="collapsePayments" class>
                                Platby
                            </a>
                        </h5>
                    </div>
                    <div id="collapsePayments" class="collapse" aria-labelledby="accordionPaymentsHeading" data-parent="#accordionPayments">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Kurz</th>
                                        <th>Částka</th>
                                        <th>Datum platby</th>
                                        <th>Stav platby</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($user->payments) === 0)
                                    <tr class="text-center">
                                        <td colspan="4">
                                            Uživatel zatím nemá žádné platby.
                                        </td>
                                    </tr>
                                @endif
                                @foreach($user->payments as $payment)
                                    <tr>
                                        <td><a href="{{ route('courses.detail', $payment->course->id) }}">{{ $payment->course->name }}</a></td>
                                        <td>{{ $payment->course->price }} Kč</td>
                                        <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d.m.Y H:i') }}</td>
                                        @switch($payment->status)
                                            @case(0)
                                            <td class="text-danger">Nezaplaceno</td>
                                            @break
                                            @case(1)
                                            <td class="text-success">Zaplaceno</td>
                                            @break
                                        @endswitch
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="accordion mt-3" id="accordionTerms">
                <div class="card">
                    <div class="card-header bg-white" id="accordionTermsHeading">
                        <h5 class="font-weight-bold my-2">
                            <a href="#" data-toggle="collapse" data-target="#collapseTerms" aria-expanded="false" aria-controls="collapseTerms" class>
                                Navštívené termíny
                            </a>
                        </h5>
                    </div>
                    <div id="collapseTerms" class="collapse" aria-labelledby="accordionTermsHeading" data-parent="#accordionTerms">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Název kurzu</th>
                                        <th>Datum konání termínu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($user->terms) === 0)
                                    <tr class="text-center">
                                        <td colspan="2">
                                            Uživatel není přihlášen na žádný termín.
                                        </td>
                                    </tr>
                                @endif
                                @foreach($user->terms as $term)
                                    <tr>
                                        <td><a href="{{ route('courses.detail', $term->course->id) }}">{{ $term->course->name }}</a></td>
                                        <td><a href="{{ route('terms.detail', $term->id) }}">{{ \Carbon\Carbon::parse($term->date)->format('d.m.Y H:i') }}</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
