<x-install title="Instalace">

    @if(Illuminate\Support\Facades\Session::has('message'))
        <div id="messageAlert" class="alert alert-danger m-2">
            {{ Illuminate\Support\Facades\Session::get('message') }}
        </div>
    @endif
    <form method="post" action="{{ route('install') }}">
        @csrf
        <div class="mb-3">
            <label class="form-label">Název webu</label>
            <input name="appname" class="form-control" value="{{ old('appname') }}" type="text"
                   title="Název vašeho webu (bude se zobrazovat na webu)" data-toggle="tooltip" data-placement="top">
            @error('appname')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">URL adresa webu</label>
            <input name="appurl" class="form-control" value="{{ old('appurl') }}" type="text"
                   title="Zadejte celou URL adresu, kde se nachází váš web" data-toggle="tooltip" data-placement="top">
            @error('appurl')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <h5 class="h5">Připojení k databázi</h5>
        <div class="mb-3">
            <label class="form-label">Databázový server</label>
            <input name="hostname" class="form-control" value="{{ old('hostname') }}" type="text"
                   title="Adresa databázového serveru" data-toggle="tooltip" data-placement="top">
            @error('hostname')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Port</label>
            <input name="hostport" class="form-control" value="{{ old('hostport') }}" type="text"
                   title="Port databázového serveru" data-toggle="tooltip" data-placement="top">
            @error('hostport')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Uživatelské jméno</label>
            <input name="username" class="form-control" value="{{ old('username') }}" type="text">
            @error('username')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label>Heslo</label>
            <input name="db_password" class="form-control" value="{{ old('db_password') }}" type="password">
            @error('db_password')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label class="form-label">Název databáze</label>
            <input name="dbname" class="form-control" value="{{ old('dbname') }}" type="text"
                   title="Název již vytvořené databáte na serveru, kam se budou ukládat data aplikace" data-toggle="tooltip" data-placement="top">
            @error('dbname')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <h5 class="h5">Vytvoření administrátorského účtu</h5>
        <div class="mb-3">
            <label for="first_name" class="form-label">Jméno</label>
            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}">

            @error('first_name')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="last_name" class="form-label">Přijmení</label>
            <input type="last_name" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}">

            @error('last_name')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">

            @error('email')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Heslo</label>
            <input type="password" class="form-control" id="password" name="password">

            @error('password')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-3">
            <label for="password_confirmation" class="form-label">Potvrdit heslo</label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">

            @error('password_confirmation')
            <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <button class="btn btn-primary float-end" type="submit">Odeslat</button>
    </form>
</x-install>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/theme.js') }}"></script>
<script>
    setTimeout(function (){
        $('#messageAlert').hide();
    }, 2000);
</script>
