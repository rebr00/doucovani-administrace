<div class="row justify-content-center p-3">
    <img src="{{ $qr->getQRCodeImage(false, 200) }}" style="max-width: 400px">
</div>
<div class="row justify-content-center">
    <div class="col-6">
        <table class="table">
            <tr>
                <td class="fw-bold">
                    Číslo účtu
                </td>
                <td>
                    {{ $bank->value }}
                </td>
            </tr>
            <tr>
                <td class="fw-bold">
                    Částka
                </td>
                <td>
                    {{ $course->price }} Kč
                </td>
            </tr>
            <tr>
                <td class="fw-bold">
                    Variabilní symbol
                </td>
                <td>
                    {{ $referenceNumber }}
                </td>
            </tr>
            <tr>
                <td class="fw-bold">
                    Poznámka
                </td>
                <td>
                    Platba kurzu: {{ $course->name }}, {{ $user->first_name." ".$user->last_name }}
                </td>
            </tr>
        </table>
    </div>
</div>
