@extends('layouts.main', ['title' => 'Nový kurz'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('courses.create') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                <label for="image" class="form-label">Obrázek kurzu</label>
                                <input type="file" name="image" class="form-control">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="name" class="form-label">Název kurzu <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="lesson" class="form-check-label">Předem vytvořené lekce</label>
                                <input type="checkbox"  class="form-check-input" id="checkboxLesson" name="lesson"
                                       data-toggle="tooltip" data-placement="top" title="Kurz bude formou předem vytvořených lekcí, které si může student zobrazit">
                            </div>

                            <div class="mb-3">
                                @if($bank !== null && !empty($bank))
                                    <label for="priced" class="form-check-label">Nastavit cenu pro kurz</label>
                                    <input type="checkbox" class="form-check-input" id="checkboxPriced" name="priced"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Pro přihlášení se na termín nebo zobrazení lekcí, musí student přes web nejdříve zaplatit za kurz">
                                @else
                                    <label class="text-secondary fst-italic">Pokud chcete nastavit cenu pro kurz, nastavte v sekci "<a href="{{ route('settings') }}">Nastavení->Aplikace</a>" váš bankovní účet</label>
                                @endif
                            </div>

                            <div class="mb-3 price">
                                <label for="price" class="form-label">Cena (v Kč)</label>
                                <input type="number" class="form-control" name="price" id="price" value="{{ old('price') }}">
                                @error('price')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="description" class="form-label">Obsah<span class="text-danger">*</span></label>
                                <textarea class="form-control wysiwyg" name="description">
                                    {{ old('description') }}
                                </textarea>
                                @error('description')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="row justify-content-between">
                            <div class="col">
                                <a href="{{ route('courses') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.price').hide();
        $('#checkboxPriced').change(function (){
            if($('#checkboxPriced').is(':checked')){
                $('.price').show();
            }else{
                $('.price').hide();
            }
        });

    </script>

    <x-wysiwyg></x-wysiwyg>
@endsection

