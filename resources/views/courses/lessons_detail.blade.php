@extends('layouts.main', ['title' => 'Detail lekce'])

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 text-right">
            <a href="{{ route('lessons.edit', $lesson->id) }}"><button class="btn btn-primary"><i class="bi bi-pencil-fill mr-1"></i>Upravit</button></a>
            <form class="d-inline" action="{{ route('lessons.delete', $lesson->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger"><i class="bi bi-trash mr-1"></i>Odstranit</button>
            </form>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    <table class="table">
                        <tr>
                            <td>
                                <label class="font-weight-bold mr-2">Název</label>
                                <span>{{ $lesson->title }}</span>
                            </td>
                            <td>
                                <label class="font-weight-bold mr-2">Pořadí</label>
                                <span>{{ $lesson->order }}</span>
                            </td>
                        </tr>
                    </table>
                    <div class="ml-2">
                        <label class="font-weight-bold mr-2">Obsah</label><br>
                        <span>{!! $lesson->body!!} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

