@extends('layouts.main', ['title' => 'Termíny kurzů'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12">
            @if(isset($old))
                <a href="{{ route('terms') }}" class="btn btn-secondary float-start">Zobrazit budoucí termíny</a>
            @else
                <a href="{{ route('terms.old') }}" class="btn btn-secondary float-start">Zobrazit historii termínů</a>
            @endif
            <a href="{{ route('terms.create') }}" class="btn btn-primary float-end">+ Přidat termín</a>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('course.name','Kurz')</th>
                                <th scope="col">@sortablelink('date','Datum konání')</th>
                                <th scope="col">@sortablelink('capacity','Kapacita')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($terms->count() < 1)
                                <tr class="text-center">
                                    <td colspan="4">Dosud nebyl vytvořen žádný termín</td>
                                </tr>
                            @endif
                            @foreach($terms as $term)
                                <tr @if(\Carbon\Carbon::parse($term->date) <= \Carbon\Carbon::now()) class="text-secondary" @endif>
                                    <td scope="row"><a title="Detail" href="{{ route('terms.detail', $term) }}" class="p-2">{{ $term->course->name }}</a></td>
                                    <td>{{ \Carbon\Carbon::parse($term->date)->format('d.m.Y H:i') }}</td>
                                    <td>{{ $term->users->count()."/".$term->capacity }}</td>
                                    <td class="text-right">
                                        <a title="Zobrazit na webu" href="{{ route('web.terms', $term->course) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                        @if(!isset($old))
                                            <a title="Editace" href="{{ route('terms.edit', $term) }}" class="p-2"> <i class="bi bi-pencil-square"></i></a>
                                            <form class="d-inline" action="{{ route('terms.delete', $term) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $terms->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#courses').addClass('show');
        $('#termLink').addClass('active');
    </script>
@endsection
