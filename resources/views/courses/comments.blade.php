@extends('layouts.main', ['title' => 'Komentáře'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('lesson.title', 'Lekce')</th>
                                <th scope="col">@sortablelink('user.last_name', 'Student')</th>
                                <th scope="col">@sortablelink('body', 'Komentář')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($comments->count() < 1)
                                <tr class="text-center">
                                    <td colspan="4">Dosud nebyl vytvořen žádný kurz</td>
                                </tr>
                            @endif
                            @foreach($comments as $comment)
                                <tr>
                                    <td scope="row"><a href="{{ route('lessons.detail', $comment->lesson) }}">{{ $comment->lesson->title }}</a></td>
                                    <td>{{ $comment->user->first_name.' '.$comment->user->last_name }}</td>
                                    <td>{{ \Illuminate\Support\Str::limit( $comment->body, 50, '...') }}</td>
                                    <td class="text-right">
                                        <a title="Zobrazit na webu" href="{{ route('web.lesson.detail', $comment->lesson) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                        <form class="d-inline" action="{{ route('comments.delete', $comment) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $comments->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#courses').addClass('show');
        $('#commentLink').addClass('active');
    </script>
@endsection
