@extends('layouts.main', ['title' => 'Kurzy'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12">
            <a href="{{ route('courses.create') }}" class="btn btn-primary float-end">+ Přidat kurz</a>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('name', 'Název')</th>
                                <th scope="col">@sortablelink('price', 'Cena')</th>
                                <th scope="col">@sortablelink('lesson', 'Lekce')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($courses->count() < 1)
                                <tr class="text-center">
                                    <td colspan="4">Dosud nebyl vytvořen žádný kurz</td>
                                </tr>
                            @endif
                            @foreach($courses as $course)
                                <tr>
                                    <td scope="row"><a title="Detail" href="{{ route('courses.detail', $course) }}" class="p-2">{{ $course->name }}</a></td>
                                    <td>{{ $course->price ? $course->price.' Kč' : 'Nebyly nastaveny platby' }}</td>
                                    <td>{{ $course->lesson ? 'Ano' : 'Ne' }}</td>
                                    <td class="text-right">
                                        <a title="Zobrazit na webu" href="{{ route('web.courses.detail', $course) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                        <a title="Editace" href="{{ route('courses.edit', $course) }}" class="p-2"> <i class="bi bi-pencil-square"></i></a>
                                        <form class="d-inline" action="{{ route('courses.delete', $course) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $courses->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#courses').addClass('show');
        $('#courseLink').addClass('active');
    </script>
@endsection
