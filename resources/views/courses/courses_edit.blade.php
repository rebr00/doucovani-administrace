@extends('layouts.main', ['title' => 'Editace kurzu'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('courses.edit', $course->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                @if($course->image !== null)
                                    <img src="{{ asset('files/img/course/'. $course->image) }}" class="img-thumbnail my-3" width="150"><br>
                                @endif
                                <label for="image" class="form-label">Nahrát nový obrázek</label><br>
                                <input type="file" class="form-control" name="image" value="{{ $course->name }}">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="name" class="form-label">Název kurzu <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" value="{{ $course->name }}">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="lesson" class="form-check-label">Předem nahrané lekce</label>
                                <input type="checkbox" {{ $course->lesson ? 'checked' : '' }} class="form-check-input" id="checkboxLesson" name="lesson">
                            </div>

                            <div class="mb-3">
                                @if($bank !== null && !empty($bank->value))
                                    <label for="priced" class="form-check-label">Nastavit cenu pro kurz</label>
                                    <input type="checkbox" {{ $course->price === null ? '' : 'checked' }} class="form-check-input" id="checkboxPriced" name="priced">
                                @else
                                    <label class="text-secondary fst-italic">Pokud chcete nastavit cenu pro kurz, nastavte v sekci "<a href="{{ route('settings') }}">Nastavení->Aplikace</a>" váš bankovní účet</label>
                                @endif
                            </div>

                            <div class="mb-3 price">
                                <label for="price" class="form-label">Cena (v Kč)</label>
                                <input type="number" class="form-control" name="price" id="price" value="{{ $course->price }}">
                                @error('price')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="description" class="form-label">Obsah<span class="text-danger">*</span></label>
                                <textarea class="form-control wysiwyg" name="description">
                                    {{ $course->description }}
                                </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col">
                                <a href="{{ route('courses') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        if($('#checkboxPriced').is(':checked')){
            $('.price').show();
        }else{
            $('.price').hide();
        }
        $('#checkboxPriced').change(function (){
            if($('#checkboxPriced').is(':checked')){
                $('.price').show();
            }else{
                $('.price').hide();
            }
        });
    </script>

    <x-wysiwyg></x-wysiwyg>
@endsection

