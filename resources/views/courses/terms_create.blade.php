@extends('layouts.main', ['title' => 'Nový termín'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('terms.create') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                <label for="name" class="form-label">Kurz<span class="text-danger">*</span></label>
                                <select name="course" class="form-control" aria-label="Vybrání kurzu">
                                    <option selected hidden>Přiřaďte termínu kurz</option>
                                    @foreach($courses as $course)
                                        <option value="{{ $course->id }}" @if($courseDetail == $course) selected @endif>{{ $course->name }}</option>
                                    @endforeach
                                </select>
                                @error('course')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="date" class="form-label">Datum konání<span class="text-danger">*</span></label>
                                <input type="datetime-local" class="form-control" min="{{ \Carbon\Carbon::now()->format('Y-m-d').'T'.\Carbon\Carbon::now()->format('H:i') }}"
                                       id="date" name="date" value="{{ old('date') }}">
                                @error('date')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="capacity" class="form-check-label">Kapacita<span class="text-danger">*</span></label>
                                <input type="number" min="1" class="form-control" id="capacity" name="capacity" value="{{ old('capacity') }}"
                                       data-toggle="tooltip" data-placement="top" title="Maximálná počet studentů, kteří se budou moci na termín přihlásit">
                                @error('capacity')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="address" class="form-check-label">Místo konání</label>
                                <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}"
                                       data-toggle="tooltip" data-placement="top" title="Zadejte fyzickou adresu nebo online platformu, přes kterou bude doučování probíhat">
                                @error('address')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="note" class="form-label">Poznámka</label>
                                <textarea class="form-control wysiwyg" name="note">
                                    {{ old('note') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col">
                                <a href="{{ route('terms') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <x-wysiwyg></x-wysiwyg>
@endsection

