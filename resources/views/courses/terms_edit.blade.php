@extends('layouts.main', ['title' => 'Editace termínu'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('terms.edit', $term->id ) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                <label for="date" class="form-label">Datum konání<span class="text-danger">*</span></label>
                                <input type="datetime-local" class="form-control" min="{{ \Carbon\Carbon::now()->format('Y-m-d').'T'.\Carbon\Carbon::now()->format('H:i') }}"
                                       id="date" name="date" value="{{ \Carbon\Carbon::parse($term->date)->format('Y-m-d').'T'.\Carbon\Carbon::parse($term->date)->format('H:i') }}">
                                @error('date')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="capacity" class="form-check-label">Kapacita<span class="text-danger">*</span></label>
                                <input type="number" min="1" class="form-control" id="capacity" name="capacity" value="{{ $term->capacity }}">
                                @error('capacity')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="address" class="form-check-label">Místo konání</label>
                                <input type="text" class="form-control" id="address" name="address" value="{{ $term->address }}"
                                       data-toggle="tooltip" data-placement="top" title="Zadejte fyzickou adresu nebo online platformu, přes kterou bude doučování probíhat">
                                @error('address')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="note" class="form-label">Poznámka</label>
                                <textarea class="form-control wysiwyg" name="note">
                                    {{ $term->note }}
                                </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col">
                                <a href="{{ route('terms') }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <x-wysiwyg></x-wysiwyg>
@endsection

