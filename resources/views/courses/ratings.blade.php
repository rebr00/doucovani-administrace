@extends('layouts.main', ['title' => 'Hodnocení'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('course.name','Kurz')</th>
                                <th scope="col">@sortablelink('user.last_name','Student')</th>
                                <th scope="col">@sortablelink('score','Hodnocení')</th>
                                <th scope="col">@sortablelink('body','Text hodnocení')</th>
                                <th scope="col">@sortablelink('created_at','Datum přidání')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($ratings->count() < 1)
                                <tr class="text-center">
                                    <td colspan="5">Dosud nebyl ohodnocen žádný kurz</td>
                                </tr>
                            @endif
                            @foreach($ratings as $rating)
                                <tr>
                                    <td scope="row"><a href="{{ route('courses.detail', $rating->course) }}">{{ $rating->course->name }}</a></td>
                                    <td><a href="{{ route('users.detail', $rating->user) }}">{{ $rating->user->first_name." ".$rating->user->last_name }}</a></td>
                                    <td>{{ $rating->score }}</td>
                                    <td>{{ $rating->text ?? 'Bez slovního hodnocení' }}</td>
                                    <td>{{ \Carbon\Carbon::parse($rating->created_at)->format('d.m.Y H:i') }}</td>
                                    <td class="text-right">
                                        <a title="Zobrazit na webu" href="{{ route('web.ratings', $rating->course) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                        <form class="d-inline" action="{{ route('ratings.delete', $rating) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $ratings->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#courses').addClass('show');
        $('#ratingLink').addClass('active');
    </script>
@endsection
