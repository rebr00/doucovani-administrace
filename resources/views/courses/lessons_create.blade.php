@extends('layouts.main', ['title' => 'Nová lekce'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-8">
                    <form action="{{ route('lessons.create', $course) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="mb-3">
                                <label for="title" class="form-label">Název lekce<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                                @error('title')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="order" class="form-check-label">Pořadí lekce<span class="text-danger">*</span></label>
                                <input type="number" min="1" class="form-control w-25" name="order"
                                       @if($course->lessons()->count() === 0)
                                            value="1"
                                       @else
                                            value="{{ $course->lessons()->max('order') + 1 }}"
                                       @endif
                                       value="{{ old('order') }}"
                                       data-toggle="tooltip" data-placement="top" title="Pořadí lekce, podle kterého se bude zobrazovat v seznamu [číslo]">
                                @error('order')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="body" class="form-label">Obsah<span class="text-danger">*</span></label>
                                <p class="text-secondary fst-italic">
                                    Do lekce lze vložit video pokud v editoru kliknete na <i class="bi bi-file-play"></i> "Vložit/Upravit média" v liště nástrojů textového editoru.<br>
                                    V záložce "Obecné" lze vložit odkaz na video na serveru YouTube nebo Vimeo. V záložce "Vložit" lze vložit formulář (např. Google Forms) pokud vložíte do pole odkaz ve formátu &lt;iframe&gt;URL dotazníku&lt;/iframe&gt;
                                </p>
                                <textarea class="form-control wysiwyg mt-3" name="body">
                                    {{ old('body') }}
                                </textarea>
                                @error('body')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="mt-3">
                                <label for="file" class="form-label">Nahrát soubor</label>
                                <button type="button" id="btnAddFile" class="mb-3 btn btn-primary float-end">Přidat další soubor</button>
                                <div class="files">
                                    <input class="form-control mb-3" type="file" name="attach[]">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-between">
                            <div class="col">
                                <a href="{{ route('courses.detail', $course) }}" class="btn btn-danger">Zrušit</a>
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-success">Uložit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <x-wysiwyg></x-wysiwyg>

    <script>
        $('#btnAddFile').click(function (){
            $('.files').append('<input class="form-control  mb-3" type="file" name="attach[]">');
        });
    </script>
@endsection

