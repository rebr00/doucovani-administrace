@extends('layouts.main', ['title' => 'Platby'])

@section('content')
    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif

                    @if($payments)
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">@sortablelink('course.name', 'Kurz')</th>
                                        <th scope="col">@sortablelink('user.last_name', 'Student')</th>
                                        <th scope="col">@sortablelink('payment.amount', 'Částka')</th>
                                        <th scope="col">@sortablelink('status', 'Stav platby')</th>
                                        <th scope="col">@sortablelink('reference_number', 'Variabilní symbol')</th>
                                        <th scope="col">@sortablelink('created_at', 'Datum vytvoření platby')</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($payments->count() < 1)
                                        <tr class="text-center">
                                            <td colspan="7">Dosud nebyla vytvořena žádná platba</td>
                                        </tr>
                                    @endif
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td scope="row">
                                                @if($payment->course)
                                                    <a href="{{ route('courses.detail', $payment->course) }}">{{ $payment->course->name }}</a>
                                                @else
                                                    Odstraněný kurz
                                                @endif
                                            </td>
                                            <td>
                                                @if($payment->user)
                                                    <a href="{{ route('users.detail', $payment->user) }}">{{ $payment->user->first_name.' '.$payment->user->last_name }}</a>
                                                @else
                                                    <span>Odstraněný uživatel</span>
                                                @endif
                                            </td>
                                            <td>{{ $payment->amount }} Kč</td>
                                            @switch($payment->status)
                                                @case(0)
                                                <td><span class="badge bg-danger">Nezaplaceno</span></td>
                                                @break
                                                @case(1)
                                                <td><span class="badge bg-success">Zaplaceno</span></td>
                                                @break
                                            @endswitch
                                            <td>{{ $payment->reference_number }}</td>
                                            <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d.m.Y H:i') }}</td>
                                            <td class="text-center">

                                                @if($payment->status === 1)
                                                    <form class="d-inline" method="post" action="{{ route('payments.deny') }}">
                                                        @csrf
                                                        <input type="number" class="invisible" name="payment" value="{{ $payment->id }}">
                                                        <button type="submit" class="btn btn-sm btn-danger"><i class="bi bi-x"></i> Zrušit potvrzení</button>
                                                    </form>
                                                @endif
                                                @if($payment->status === 0)
                                                    <form class="d-inline" method="post" action="{{ route('payments.confirm') }}">
                                                        @csrf
                                                        <input type="number" class="invisible" name="payment" value="{{ $payment->id }}">
                                                        <button type="submit" class="btn btn-sm btn-success"><i class="bi bi-check"></i> Potvrdit platbu</button>
                                                    </form>
                                                @endif
                                                <form class="d-inline" action="{{ route('payments.delete', $payment) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="row justify-content-end">
                                <div class="float-right">
                                    {!! $payments->appends(\Request::except('page'))->render() !!}
                                </div>
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#courses').addClass('show');
        $('#paymentLink').addClass('active');
    </script>
@endsection
