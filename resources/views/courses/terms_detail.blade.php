@extends('layouts.main', ['title' => 'Detail termínu'])

@section('content')
    @if(Illuminate\Support\Facades\Session::has('message'))
        <div id="messageAlert" class="alert alert-info m-2">
            {{ Illuminate\Support\Facades\Session::get('message') }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-12 text-right">
            <a href="{{ route('terms.edit', $term) }}"><button class="btn btn-primary"><i class="bi bi-pencil-fill mr-1"></i>Upravit</button></a>
            <form class="d-inline" action="{{ route('terms.delete', $term) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger"><i class="bi bi-trash mr-1"></i>Odstranit</button>
            </form>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <label  class="font-weight-bold mr-2">Pro kurz</label>
                                <span>{{ $term->course->name }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="font-weight-bold mr-2">Datum konání</label>
                                <span>{{ \Carbon\Carbon::parse($term->date)->format('d.m.Y H:i') }}</span>
                            </td>
                            @if($term->address)
                                <td>
                                    <label class="font-weight-bold mr-2">Místo konání</label>
                                    <span>{{ $term->address }}</span>
                                </td>
                            @endif
                            <td>
                                <label class="font-weight-bold mr-2">Kapacita</label>
                                <span>{{ $term->capacity }}</span>
                            </td>
                        </tr>
                    </table>

                    <div class="ml-2">
                        <label class="font-weight-bold mr-2">Poznámka</label><br>
                        <span>{!! $term->note !!} </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Sekce přihlášených studentů -->
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="accordion mt-3" id="accordionUsers">
                <div class="card">
                    <div class="card-header bg-white" id="accordionUsersHeading">
                        <h5 class="font-weight-bold my-2">
                            <a href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="false" aria-controls="collapseUsers" class>
                                Přihlášení studenti {{ $term->users->count()."/".$term->capacity }}
                            </a>
                        </h5>
                    </div>
                    <div id="collapseUsers" class="collapse show" aria-labelledby="accordionUsersHeading" data-parent="#accordionUsers">
                        <div class="card-body">
                            @if(count($users) > 0)
                                <button class="btn btn-sm btn-success float-end" data-toggle="modal" data-target="#attachUserModal">+ Přidat studenta</button>
                            @endif
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Student</th>
                                        <th>Datum přihlášení na termín</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($term->users) === 0)
                                        <tr class="text-center">
                                            <td colspan="3">Na termín není přihlášený žádný student</td>
                                        </tr>
                                    @endif
                                    @foreach($term->users->sortByDesc('created_at') as $user)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><a href="{{ route('users.detail', $user->id) }}">{{ $user->first_name." ".$user->last_name }}</a></td>
                                            <td>{{ \Carbon\Carbon::parse($user->pivot->created_at)->format('d.m.Y H:i') }}</td>
                                            <td>
                                                <form class="d-inline" action="{{ route('terms.detach_user', ['term' => $term->id, 'user' => $user->id]) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="attachUserModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Přidat uživatele na termín</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formAttach" action="{{ route('terms.attach_user', $term->id) }}" method="post">
                        @csrf
                        <label for="email" class="form-label">Zadejte email uživatele</label>
                        <input type="email" name="email" class="form-control">
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="btnAttach" type="submit" class="btn btn-sm btn-success">Uložit</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#btnAttach').click(function (){
            $('#formAttach').submit();
        });
    </script>
@endsection
