@extends('layouts.main', ['title' => 'Detail kurzu'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12 text-right">
            <a href="{{ route('courses.edit', $course) }}"><button class="btn btn-primary"><i class="bi bi-pencil-fill mr-1"></i>Upravit</button></a>
            <form class="d-inline" action="{{ route('courses.delete', $course) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger"><i class="bi bi-trash mr-1"></i>Odstranit</button>
            </form>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    <table class="table">
                        @if($course->image !== null)
                            <tr>
                                <td colspan="2">
                                    <label  class="font-weight-bold mr-2">Obrázek</label><br>
                                    <img src="{{ asset('files/img/course/'.$course->image) }}" class="img-thumbnail my-2" width="150">
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td colspan="2">
                                <label  class="font-weight-bold mr-2">Název</label>
                                <span>{{ $course->name }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="font-weight-bold mr-2">Předem nahrané lekce</label>
                                <span>{{ $course->lesson ? 'Ano' : 'Ne' }}</span>
                            </td>
                            <td>
                                <label class="font-weight-bold mr-2">Cena</label>
                                <span>{{ $course->price ?? 'Nezpoplatněno' }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label class="font-weight-bold mr-2">Datum vytvoření</label>
                                <span>{{ $course->created_at->format('d.m.Y') }}</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-12">
            @if($course->lesson)
                <div class="accordion mt-3" id="accordionLessons">
                    <div class="card">
                        <div class="card-header bg-white" id="accordionLessonsHeading">
                            <h5 class="font-weight-bold my-2">
                                <a href="#" data-toggle="collapse" data-target="#collapseLessons" aria-expanded="false" aria-controls="collapseLessons" class>
                                    Lekce
                                </a>
                            </h5>
                        </div>
                        <div id="collapseLessons" class="collapse show" aria-labelledby="accordionLessonsHeading" data-parent="#accordionLessons">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-10">
                                        @if(Illuminate\Support\Facades\Session::has('message'))
                                            <div id="messageAlert" class="alert alert-info m-2">
                                                {{ Illuminate\Support\Facades\Session::get('message') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-2">
                                        <a class="btn btn-sm btn-success float-end" href="{{ route('lessons.create', $course) }}">+ Přidat lekci</a>
                                    </div>
                                </div>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>@sortablelink('order','#')</th>
                                        <th>@sortablelink('title','Název')</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($course->lessons) === 0)
                                        <tr class="text-center">
                                            <td colspan="3">Pro tento kurz není vytvořena žádná lekce</td>
                                        </tr>
                                    @endif
                                    @foreach($lessons as $lesson)
                                        <tr>
                                            <td>{{ $lesson->order }}</td>
                                            <td><a href="{{ route('lessons.detail', $lesson) }}">{{ $lesson->title }}</a></td>
                                            <td class="text-right">
                                                <a title="Detail" href="{{ route('lessons.detail', $lesson) }}" class="p-2"> <i class="bi bi-eye"></i></a>
                                                <a title="Editace" href="{{ route('lessons.edit', $lesson) }}" class="p-2"> <i class="bi bi-pencil-square"></i></a>
                                                <form class="d-inline" action="{{ route('lessons.delete', $lesson) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="accordion mt-3" id="accordionTerms">
                    <div class="card">
                        <div class="card-header bg-white" id="accordionTermsHeading">
                            <h5 class="font-weight-bold my-2">
                                <a href="#" data-toggle="collapse" data-target="#collapseTerms" aria-expanded="false" aria-controls="collapseTerms" class>
                                    Termíny
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTerms" class="collapse show" aria-labelledby="accordionTermsHeading" data-parent="#accordionTerms">
                            <div class="card-body">
                                <a class="btn btn-sm btn-success float-end" href="{{ route('terms.create', ['course' => $course]) }}">+ Přidat termín</a>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Datum</th>
                                        <th>Kapacita</th>
                                        <th>Popis</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($terms) === 0)
                                        <tr class="text-center">
                                            <td colspan="3">Pro tento kurz není vytvořen žádný termín</td>
                                        </tr>
                                    @endif
                                    @foreach($terms as $term)
                                        <tr>
                                            <td><a href="{{ route('terms.detail', $term->id) }}">{{ \Carbon\Carbon::parse($term->date)->format('d.m.Y H:i') }}</a></td>
                                            <td>{{ $term->users->count()."/".$term->capacity }}</td>
                                            <td>{{ $term->note ? \Illuminate\Support\Str::limit($term->note, 20, '...') : '-' }}</td>
                                            <td>
                                                <form class="d-inline" action="{{ route('terms.delete', ['term' => $term, 'course' => $course]) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
@endsection
