@extends('layouts.main', ['title' => 'Nastavení'])

@section('content')

    <div class="row">
        @if(Illuminate\Support\Facades\Session::has('message'))
            <div id="messageAlert" class="alert alert-info m-2">
                {{ Illuminate\Support\Facades\Session::get('message') }}
            </div>
        @endif

        <!-- List sekce -->
        <div class="col-md-3 col-xl-2">
            <div class="card">
                <div class="card-header">
                    <div class="card-title mb-0 font-weight-bold">Nastavení</div>
                </div>
                <div class="list-group list-group-flush" role="tablist">
                    <a class="list-group-item list-group-item-action active" data-toggle="list" href="#app_settings" role="tab"><i class="bi bi-terminal"></i> Aplikace</a>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#contact_settings" role="tab"><i class="bi bi-person-lines-fill"></i> Kontaktní informace</a>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#email_settings" role="tab"><i class="bi bi-mailbox"></i> Email</a>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#style_settings" role="tab"><i class="bi bi-palette"></i> Vzhled</a>
                    <a class="list-group-item list-group-item-action" data-toggle="list" href="#term_settings" role="tab"><i class="bi bi-file-text"></i> Obchodní podmínky</a>
                </div>
            </div>
        </div>

        <!-- Obsah sekce -->
        <div class="col-md-9 col-xl-10">
            <div class="tab-content">

                <!-- Aplikace -->
                <div class="tab-pane fade active show" id="app_settings" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Nastavení aplikace</h5>
                            <form method="post" action="{{ route('settings.app') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                    <label for="app_name" class="form-label">Název aplikace</label>
                                    <input type="text" class="form-control" name="app_name" value="{{ $settings->get('APP_NAME') }}">
                                    @error('app_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    @if($settings->get('APP_LOGO'))
                                        <img class="img-thumbnail" width="150" src="{{ asset('files/img/logo/'. $settings->get('APP_LOGO')) }}"><br>
                                    @endif
                                    <label for="app_logo" class="form-label">Logo aplikace</label>
                                    <br><span class="text-secondary fst-italic">Velikost nahrávaného souboru nesmí překročit limit nastavený na hostingu.</span>
                                    <input type="file" class="form-control" name="app_logo" value="{{ "x" }}">
                                    @error('app_logo')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <hr>
                                <div class="mb-3">
                                    @if($settings->get('APP_ICON'))
                                        <img class="img-thumbnail" width="100" src="{{ asset('favicon.ico') }}"><br>
                                    @endif
                                    <label for="app_icon" class="form-label">Ikona webu</label>
                                    <br><span class="text-secondary fst-italic">Ikonu ve formátu .ico můžete například vygenerovat z obrázku pomoci online nástrojů dostupných zdarma</span>
                                    <input type="file" class="form-control" name="app_icon" value="{{ "x" }}"
                                           title="Ikonka v záložce prohlížeče, nahrajte soubor s přílohou .ico" data-toggle="tooltip" data-placement="top">
                                    @error('app_icon')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <hr>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col-4">
                                            <label for="app_bank_prefix" class="form-label">Předčíslí bankovního účtu</label>
                                            <input type="number" class="form-control" name="app_bank_prefix" value="{{ $bankPrefix ? $bankPrefix[0] : null }}"
                                                   placeholder="XXXXXX" title="Zadejte ve tvaru XXXXXX" data-toggle="tooltip" data-placement="top">
                                            @error('app_bank_prefix')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-4">
                                            <label for="app_bank" class="form-label">Číslo bankovního účtu</label>
                                            <input type="text" class="form-control" name="app_bank" value="{{ $bankNumber }}"
                                                   placeholder="XXXXXXXXXX" title="Zadejte ve tvaru XXXXXXXXXX" data-toggle="tooltip" data-placement="top">
                                            @error('app_bank')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="col-4">
                                            <label for="app_bank_code" class="form-label">Kód banky</label>
                                            <input type="number" class="form-control" name="app_bank_code" value="{{ $bankCode ? $bankCode[1] : null }}"
                                                   placeholder="XXXX" title="Zadejte ve tvaru XXXX" data-toggle="tooltip" data-placement="top">
                                            @error('app_bank_code')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col text-right">
                                        <button type="submit" class="btn btn-sm btn-primary">Uložit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Kontaktní informace -->
                <div class="tab-pane fade" id="contact_settings" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Kontaktní informace</h5>
                            <form method="post" action="{{ route('settings.contact') }}">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_address" class="form-label">Adresa</label>
                                            <input type="text" class="form-control" name="contact_address" value="{{ $settings->get('CONTACT_ADDRESS') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_person" class="form-label">Kontaktní osoba</label>
                                            <input type="text" class="form-control" name="contact_person" value="{{ $settings->get('CONTACT_PERSON') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_phone" class="form-label">Telefonní číslo</label>
                                            <input type="text" class="form-control" name="contact_phone" value="{{ $settings->get('CONTACT_PHONE') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-title">Sociální sítě</div>
                                <div class="text-secondary fst-italic">Vložte vždy celou URL adresu profilu na sociální síťi</div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_facebook" class="form-label">Facebook</label>
                                            <input type="text" class="form-control" name="contact_facebook" value="{{ $settings->get('CONTACT_FACEBOOK') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_instagram" class="form-label">Instagram</label>
                                            <input type="text" class="form-control" name="contact_instagram" value="{{ $settings->get('CONTACT_INSTAGRAM') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_linkedin" class="form-label">LinkedIn</label>
                                            <input type="text" class="form-control" name="contact_linkedin" value="{{ $settings->get('CONTACT_LINKEDIN') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="contact_youtube" class="form-label">YouTube</label>
                                            <input type="text" class="form-control" name="contact_youtube" value="{{ $settings->get('CONTACT_YOUTUBE') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col text-right">
                                        <button type="submit" class="btn btn-sm btn-primary">Uložit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Email -->
                <div class="tab-pane fade" id="email_settings" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Nastavení emailu</h5>

                            <form method="post" action="{{ route('settings.email') }}">
                                @csrf
                                <div class="mb-3">
                                    <label for="mail_address" class="form-label">Emailová adresa</label>
                                    <input type="text" class="form-control" name="mail_address" value="{{ $settings->get('EMAIL_ADDRESS') }}">
                                    @error('mail_address')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="mail_password" class="form-label">Heslo nebo klíč</label>
                                    <input type="password" class="form-control" name="mail_password">
                                    @error('mail_password')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="mail_host" class="form-label">Server odchozí pošty (SMTP)</label>
                                    <input type="text" class="form-control" name="mail_host" value="{{ $mail_host }}">
                                    @error('mail_host')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="mail_encryption" class="form-label">Šifrování</label>
                                    <select name="mail_encryption" class="form-control">
                                        <option value="ssl" @if($mail_enc === "ssl") selected @endif>SSL</option>
                                        <option value="tls" @if($mail_enc === "tls") selected @endif>TLS</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="mail_port" class="form-label">Port</label>
                                    <input type="text" class="form-control" name="mail_port" value="{{ $mail_port }}">
                                    @error('mail_port')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="col text-right">
                                        <button type="submit" class="btn btn-sm btn-primary">Uložit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Vzhled -->
                <div class="tab-pane fade" id="style_settings" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Hlavní strana</h5>
                            <form method="post" action="{{ route('settings.style') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            @if($settings->get('STYLE_SLIDER'))
                                                <img class="img-thumbnail" width="300" src="{{ asset('files/img/slider/'. $settings->get('STYLE_SLIDER')) }}"><br>
                                            @endif
                                            <label for="style_slider" class="form-label">Fotka na hlavní straně</label>
                                            <br><span class="text-secondary fst-italic">Doporučeno nahrát tmavší obrázek.</span>
                                            <input type="file" class="form-control" name="style_slider">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_title" class="form-label">Nadpis na fotce</label>
                                            <input type="text" class="form-control" name="style_title" value="{{ $settings->get('STYLE_TITLE') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_text" class="form-label">Text na fotce</label>
                                            <input type="text" class="form-control" name="style_text" value="{{ $settings->get('STYLE_TEXT') }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-title">Sekce ikon</div>
                                <span class="text-secondary">
                                    Ikony si můžete zvolit z knihovny <a href="https://fontawesome.com/icons?d=gallery&p=3&m=free">Font Awesome</a>.
                                    Zadejte ve tvaru fa-<span class="fst-italic">název-ikony</span>. Například fa-car .
                                </span>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_title" class="form-label">Nadpis sekce ikon</label>
                                            <input type="text" class="form-control" name="style_icon_title" value="{{ $settings->get('STYLE_ICON_TITLE') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_1" class="form-label">Ikona #1</label>
                                            <input type="text" class="form-control" name="style_icon_1" value="{{ $settings->get('STYLE_ICON_1') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_1_title" class="form-label">Nadpis #1</label>
                                            <input type="text" class="form-control" name="style_icon_1_title" value="{{ $settings->get('STYLE_ICON_1_TITLE') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_1_text" class="form-label">Text #1</label>
                                            <input type="text" class="form-control" name="style_icon_1_text" value="{{ $settings->get('STYLE_ICON_1_TEXT') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_2" class="form-label">Ikona #2</label>
                                            <input type="text" class="form-control" name="style_icon_2" value="{{ $settings->get('STYLE_ICON_2') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_2_title" class="form-label">Nadpis #2</label>
                                            <input type="text" class="form-control" name="style_icon_2_title" value="{{ $settings->get('STYLE_ICON_2_TITLE') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_2_text" class="form-label">Text #2</label>
                                            <input type="text" class="form-control" name="style_icon_2_text" value="{{ $settings->get('STYLE_ICON_2_TEXT') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_3" class="form-label">Ikona #3</label>
                                            <input type="text" class="form-control" name="style_icon_3" value="{{ $settings->get('STYLE_ICON_3') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_3_title" class="form-label">Nadpis #3</label>
                                            <input type="text" class="form-control" name="style_icon_3_title" value="{{ $settings->get('STYLE_ICON_3_TITLE') }}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_icon_3_text" class="form-label">Text #3</label>
                                            <input type="text" class="form-control" name="style_icon_3_text" value="{{ $settings->get('STYLE_ICON_3_TEXT') }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-title">Sekce popis doučovatele</div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            @if($settings->get('STYLE_BIO_PHOTO'))
                                                <img class="img-thumbnail" width="250" src="{{ asset('files/img/bio/'. $settings->get('STYLE_BIO_PHOTO')) }}"><br>
                                            @endif
                                            <label for="style_bio_photo" class="form-label">Fotka</label>
                                            <input type="file" class="form-control" name="style_bio_photo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_bio_title" class="form-label">Nadpis</label>
                                            <input type="text" class="form-control" name="style_bio_title" value="{{ $settings->get('STYLE_BIO_TITLE') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label for="style_bio_text" class="form-label">Popis</label>
                                            <textarea type="text" class="form-control" name="style_bio_text">{{ $settings->get('STYLE_BIO_TEXT') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-title">Pozadí nadpisu u příspěvků</div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            @if($settings->get('MAIN_PHOTO'))
                                                <img class="img-thumbnail" width="250" src="{{ asset('files/img/main/'. $settings->get('MAIN_PHOTO')) }}"><br>
                                            @endif
                                            <label for="main_photo" class="form-label">Pozadí</label>
                                            <input type="file" class="form-control" name="main_photo">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col text-right">
                                        <button type="submit" class="btn btn-sm btn-primary">Uložit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Obchodní podmínky -->
                <div class="tab-pane fade" id="term_settings" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Obchodní podmínky</h5>
                            <span class="text-secondary fst-italic">Odkaz na obchodní podmínky bude zobrazen v patičce webu.</span>
                            <div class="card-body">
                                <form method="post" action="{{ route('settings.terms') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="site_terms" class="form-label">Text obchodních podmínek (zahrňte i Ochranu osobních údajů)</label>
                                        <textarea name="site_terms" class="form-control wysiwyg" placeholder="Zadejte text, který se zobrazí na stránce O nás">{{ $settings->get('SITE_TERMS') }}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col text-right">
                                            <button type="submit" class="btn btn-sm btn-primary">Uložit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <x-wysiwyg></x-wysiwyg>
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#settingsLink').addClass('active');
    </script>
@endsection

