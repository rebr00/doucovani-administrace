<nav id="sidebar" class="sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand text-decoration-none" href="{{ route('home')  }}">
            <span class="align-middle">Administrace</span>
        </a>

        <ul class="sidebar-nav">

            <li id="homeLink" class="sidebar-item">
                <a class="sidebar-link" href="{{ route('home') }}">
                    <i class="bi bi-house-door-fill align-middle"></i> <span class="align-middle">Hlavní stránka</span>
                </a>
            </li>

            <li id="coursesLink" class="sidebar-item">
                <a data-target="#courses" data-toggle="collapse" class="sidebar-link collapsed">
                    <i class="bi bi-book-fill align-middle"></i> <span class="align-middle">Kurzy</span>
                </a>
                <ul id="courses" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
                    <li id="courseLink" class="sidebar-item"><a class="sidebar-link" href="{{ route('courses') }}"><i class="bi bi-archive-fill"></i> Všechny kurzy</a></li>
                    <li id="termLink" class="sidebar-item"><a class="sidebar-link" href="{{ route('terms') }}"><i class="bi bi-calendar-date"></i> Termíny</a></li>
                    <li id="commentLink" class="sidebar-item"><a class="sidebar-link" href="{{ route('comments') }}"><i class="bi bi-chat-left-text-fill"></i> Komentáře u lekcí</a></li>
                    <li id="paymentLink" class="sidebar-item"><a class="sidebar-link" href="{{ route('payments') }}"><i class="bi bi-credit-card-fill"></i> Platby</a></li>
                    <li id="ratingLink" class="sidebar-item"><a class="sidebar-link" href="{{ route('ratings') }}"><i class="bi bi-star-fill"></i> Hodnocení</a></li>
                </ul>
            </li>

            <li id="newsLink" class="sidebar-item">
                <a class="sidebar-link" href="{{ route('news') }}">
                    <i class="bi bi-file-post-fill align-middle"></i> <span class="align-middle">Novinky</span>
                </a>
            </li>

            <li id="userLink" class="sidebar-item">
                <a class="sidebar-link" href="{{ route('users') }}">
                    <i class="bi bi-people-fill align-middle"></i> <span class="align-middle">Uživatelé</span>
                </a>
            </li>

            <li id="siteLink" class="sidebar-item">
                <a class="sidebar-link" href="{{ route('sites') }}">
                    <i class="bi bi-card-list align-middle"></i> <span class="align-middle">Stránky</span>
                </a>
            </li>

            <li id="emailLink" class="sidebar-item">
                <a class="sidebar-link" href="{{ route('email') }}">
                    <i class="bi bi-envelope-fill align-middle"></i> <span class="align-middle">Napsat email</span>
                </a>
            </li>
            <li id="settingsLink" class="sidebar-item">
                <a class="sidebar-link" href="{{ route('settings') }}">
                    <i class="bi bi-gear-fill align-middle"></i> <span class="align-middle">Nastavení</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
