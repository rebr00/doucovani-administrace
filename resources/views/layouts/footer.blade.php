<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-left">
                <p class="mb-0">
                    <a href="https://www.doucim.site/" class="text-muted"><strong>Doucim.site</strong></a>
                </p>
            </div>
        </div>
    </div>
</footer>
</div>
</div>

<script src="{{ asset('js/theme.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

@yield('scripts')

</body>
</html>
