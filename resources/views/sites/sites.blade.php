@extends('layouts.main', ['title' => 'Stránky'])

@section('content')

    <div class="row justify-content-center">
        <div class="col-12">
            <a href="{{ route('sites.create') }}" class="btn btn-primary float-end">+ Přidat stránku</a>
        </div>
    </div>

    <div class="alert alert-secondary my-2">
        V této sekci si můžete vytvořit nové stránky a přidat jim vlastní obsah. Stránky se následně zobrazí v menu a v patičce na klientské části.
    </div>

    <div class="card mt-3">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-12">
                    @if(Illuminate\Support\Facades\Session::has('message'))
                        <div id="messageAlert" class="alert alert-info m-2">
                            {{ Illuminate\Support\Facades\Session::get('message') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">@sortablelink('name', 'Název')</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($sites->count() < 1)
                                <tr class="text-center">
                                    <td colspan="4">Dosud nebyla vytvořena žádná stránka</td>
                                </tr>
                            @endif
                            @foreach($sites as $site)
                                <tr>
                                    <td scope="row">{{ $site->title }}</td>
                                    <td class="text-right">
                                        <a title="Editace" href="{{ route('sites.edit', $site) }}" class="p-2"> <i class="bi bi-pencil-square"></i></a>
                                        <form class="d-inline" action="{{ route('sites.delete', $site) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" title="Odstranit" class="p-2 btn btn-link"> <i class="bi bi-trash text-danger"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row justify-content-end">
                        <div class="float-right">
                            {!! $sites->appends(\Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#siteLink').addClass('active');
    </script>
@endsection
