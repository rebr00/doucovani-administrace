<?php

/**
 * @author Ronald Rebernigg
 * Soubor nastavení pro získání hodnoty instalace z .env
 */

return [
    'installed' => env('INSTALLED', false),
];
